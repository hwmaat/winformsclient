﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{
    public class BreakFromApi
    {
        public int BreakId { get; set; }
        public int ScheduleDayId { get; set; }
        public double Offset { get; set; }
        public double MaxLength { get; set; }
        public double BookedLength { get; set; }
        public string BreakType { get; set; }
        public string BreakRef { get; set; }
        public List<BookingFromApi> Bookings { get; set; }
    }

    public class BreakDto : INotifyPropertyChanged
    {
        public int BreakId { get; set; }
        public int ScheduleDayId { get; set; }
        public double Offset { get; set; }
        public DateTimeOffset BreakStartTime { get; set; }
        public TimeSpan MaxLength { get; set; }
        public TimeSpan BookedLength { get; set; }
        public TimeSpan AvailableLength { get; set; }
        public int SpotCount { get; set; }
        public double GrossValue { get; set; }
        public double BreakGrossValue { get; set; }
        public double NetValue { get; set; }
        public double BreakNetValue { get; set; }
        public string BreakType { get; set; }
        public string BreakRef { get; set; }
        private bool _HasBookingFromOrder;
        public bool HasBookingFromOrder
        {
            get { return _HasBookingFromOrder; }
            set { _HasBookingFromOrder = value;
                OnPropertyChanged();
            }
        }
        //private BindingList<BookingDTO> _Bookings;
        //public BindingList<BookingDTO> Bookings
        //{
        //    get { return _Bookings; }
        //    set
        //    {
        //        _Bookings = value;
        //        OnPropertyChanged();
        //    }
        //}
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }
    }
}
