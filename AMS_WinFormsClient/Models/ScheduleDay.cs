﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{


    public class ScheduleDayFromApi
    {
        public int ScheduleDayId { get; set; }
        public DateTimeOffset DaystartDateTime { get; set; }
        public string Status { get; set; }
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }
        public List<BreakFromApi> Breaks { get; set; }
    }

    public class ScheduleDayDTO: INotifyPropertyChanged
    {
        public int ScheduleDayId { get; set; }
        public int WeekNumber { get; set; }
        public DateTimeOffset DaystartDateTime { get; set; }
        public string Status { get; set; }
        //public int ChannelId { get; set; }
        //public string ChannelName { get; set; }

        // This can be counted in the aftermap because we have access to the breaks per scheduleday
        public double GrossValue { get; set; }

        // This can be counted in the aftermap because we have access to the breaks per scheduleday
        public double NettValue { get; set; }
        public double DayGrossValue { get; set; }
        public double DayNetValue { get; set; }
        // This can be counted in the aftermap because we have access to the breaks per scheduleday
        public TimeSpan MaxLength { get; set; }

        // This can be counted in the aftermap because we have access to the breaks per scheduleday
        public TimeSpan BookedLength { get; set; }

        // This can be counted in the aftermap because we have access to the breaks per scheduleday
        public TimeSpan AvailableLength { get; set; }
        private bool _HasBookingFromOrder;
        public bool HasBookingFromOrder {
            get { return _HasBookingFromOrder; }
            set { _HasBookingFromOrder = value;
                OnPropertyChanged();
            }
        }

        private BindingList<BreakDto> _Breaks;
        public BindingList<BreakDto> Breaks {
            get { return _Breaks; }
            set
            {
                _Breaks = value;
                OnPropertyChanged();
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }
}
}
