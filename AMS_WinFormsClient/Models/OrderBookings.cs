﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{

    public class OrderBookingFromApi //: INotifyPropertyChanged
    {
        public int BookingId { get; set; }
        public int OrderId { get; set; }
        public int BreakId { get; set; }
        public string OrderDescription { get; set; }
        public string ChannelName { get; set; }
        public DateTimeOffset BreakDateTime { get; set; }
        public int PositioninBreak { get; set; }
        public bool IsPreferredPosition { get; set; }
        public int OrderSpotGroupId { get; set; }
        public double Duration { get; set; }

        public double? StartPrice { get; set; }

        //private double? _startPrice;
        //public double? StartPrice
        //{
        //    get
        //    {
        //        return _startPrice ?? 0;
        //    }
        //    set
        //    {
        //        if (_startPrice != value)
        //        {
        //            _startPrice = value ?? 0;
        //            OnPropertyChanged();
        //        }
        //    }
        //}
        public double? BasePrice { get; set; }
        public double? Gross { get; set; }
        public double? Discount { get; set; }
        public double? Surcharge { get; set; }
        public double? Net { get; set; }

        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        //}
     }

    public class OrderBookingDTO : INotifyPropertyChanged
    {
        public int BookingId { get; set; }
        public int OrderId { get; set; }
        public int BreakId { get; set; }
        public string OrderDescription { get; set; }
        public string ChannelName { get; set; }
        public DateTimeOffset BreakDateTime { get; set; }
        public int PositioninBreak { get; set; }
        public bool IsPreferredPosition { get; set; }
        public int OrderSpotGroupId { get; set; }
        public TimeSpan Duration { get; set; }

        private double? _startPrice;
        public double? StartPrice
        {
            get
            {
                return _startPrice ?? 0;
            }
            set
            {
                if (_startPrice != value)
                {
                    _startPrice = value ?? 0;
                    OnPropertyChanged();
                }
            }
        }
        public double? BasePrice { get; set; }
        public double? Gross { get; set; }
        public double? Discount { get; set; }
        public double? Surcharge { get; set; }
        public double? Net { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }
    }
}
