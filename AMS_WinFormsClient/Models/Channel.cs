﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{
    public class ChannelFromApi
    {
        public int ChannelId { get; set; }
        public int BroadcasterId { get; set; }
        public string ChannelName { get; set; }
        public string OutletType { get; set; }

    }

    public class ChannelDropDownDTO
    {
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }

    }
}
