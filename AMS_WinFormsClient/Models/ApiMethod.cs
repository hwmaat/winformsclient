﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{
    public class ApiMethod
    {
        public string Api_RelativePath { get; set; }
        public string Api_HttpMethod { get; set; }
        public IEnumerable<ApiParameters> Api_Params { get; set; }

    }
    public class ApiParameters
    {
        public string Api_ParamName { get; set; }
        public string Api_Type { get; set; }
    }
}
