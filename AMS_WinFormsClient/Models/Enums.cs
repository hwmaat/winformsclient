﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{
    public enum BookingStatus
    {
        Requested = 1,
        Processing = 2,
        Accepted = 3,
        Rejected = 4,
        Error = 5

    }
}
