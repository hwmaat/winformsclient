﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{
    public class OrderFromApi
    {
        public int OrderId { get; set; }
        public string Description { get; set; }
        public int CampaignId { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int DemographicId { get; set; }
        public string DemographicName { get; set; }
        public string ISOCurrencyCode { get; set; }
        public List<OrderSpotGroup> OrderSpotGroups { get; set; }
        public List<OrderChannel> OrderChannels { get; set; }
    }
    public class OrderChannel
    {
        public int OrderChannelId { get; set; }
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }
        public double TargetBudget { get; set; }
        public double CostGrpGross { get; set; }
        public double CostGrpNet { get; set; }
        public double MakeGoodValue { get; set; }
    }
    public class OrderSpotGroup
    {
        public int OrderSpotGroupId { get; set; }
        public int SpotGroupId { get; set; }
        public string SpotGroupName { get; set; }
        public int ApplyIndexCorrection { get; set; }
        public double SpotGroupIndex { get; set; }
    }
    public class OrderDropDownDTO
    {
        public int OrderId { get; set; }
        public string Description { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
    //public class OrderBookingsFromApi
    //{
    //    public int BookingId { get; set; }
    //    public int OrderId { get; set; }
    //    public int BreakId { get; set; }
    //    public string OrderDescription { get; set; }
    //    public string ChannelName { get; set; }
    //    public DateTimeOffset BreakDateTime { get; set; }
    //    public int PositioninBreak { get; set; }
    //    public bool IsPreferredPosition { get; set; }
    //    public int OrderSpotGroupId { get; set; }
    //    public double Duration { get; set; }
    //    public double? StartPrice { get; set; }
    //    public double? BasePrice { get; set; }
    //    public double? Gross { get; set; }
    //    public double? Discount { get; set; }
    //    public double? Surcharge { get; set; }
    //    public double? Net { get; set; }
    //    public double? RatingsForecast { get; set; }
    //}
    //public class OrderBookingsDTO
    //{
    //    public int BookingId { get; set; }
    //    public int OrderId { get; set; }
    //    public int BreakId { get; set; }
    //    public string OrderDescription { get; set; }
    //    public string ChannelName { get; set; }
    //    public DateTimeOffset BreakDateTime { get; set; }
    //    public int PositioninBreak { get; set; }
    //    public bool IsPreferredPosition { get; set; }
    //    public int OrderSpotGroupId { get; set; }
    //    public TimeSpan Duration { get; set; }
    //    public double? StartPrice { get; set; }
    //    public double? BasePrice { get; set; }
    //    public double? Gross { get; set; }
    //    public double? Discount { get; set; }
    //    public double? Surcharge { get; set; }
    //    public double? Net { get; set; }
    //    public double? RatingsForecast { get; set; }
    //}
}
