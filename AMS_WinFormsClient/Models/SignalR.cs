﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{

    public class SignalRData
    {
        public int breakId { get; set; }
        public double Duration { get; set; }
        public double? Gross { get; set; }
        public double? Net { get; set; }
        public string UserId { get; set; }

    }
}
