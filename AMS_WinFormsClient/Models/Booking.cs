﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Models
{
    public class BookingFromApi
    {
        public int BookingId { get; set; }
        public int OrderId { get; set; }
        public int BreakId { get; set; }
        public string OrderDescription { get; set; }
        public string DemographicDescription { get; set; }
        public string ChannelName { get; set; }
        public DateTimeOffset BreakDateTime { get; set; }
        public int positioninBreak { get; set; }
        public bool isPreferredPosition { get; set; }
        public int orderSpotGroupId { get; set; }
        public double Duration { get; set; }
        public double? StartPrice { get; set; }
        public double? BasePrice { get; set; }
        public double? Gross { get; set; }
        public double? Discount { get; set; }
        public double? Surcharge { get; set; }
        public double? Net { get; set; }
        public double? RatingsForecast { get; set; }
    }
    public class BookingDTO : INotifyPropertyChanged
    {
        public int BookingId { get; set; }
        public int OrderId { get; set; }
        public int BreakId { get; set; }
        public string OrderDescription { get; set; }
        public string DemographicDescription { get; set; }
        public string ChannelName { get; set; }
        public DateTimeOffset BreakDateTime { get; set; }
        public int WeekNumber { get; set; }
        public int positioninBreak { get; set; }
        public bool isPreferredPosition { get; set; }
        public int orderSpotGroupId { get; set; }
        public TimeSpan Duration { get; set; }
        public double? StartPrice { get; set; }
        public double? BasePrice { get; set; }
        public double? Gross { get; set; }
        public double? Discount { get; set; }
        public double? Surcharge { get; set; }
        public double? Net { get; set; }
        public double? RatingsForecast { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }
    }
    public class BookingRequestDTO
    {
        public int OrderId { get; set; }
        public int BreakId { get; set; }
        public int SpotGroupId { get; set; }
        public int Orderspotgroupid { get; set; }

    }
    public class BookingsAggregateDTO
    {
        public object Key { get; set; }
        public int Count { get; set; }
        public double? TotalGross { get; set; }
        public double? TotalNet { get; set; }

    }
}
