﻿using AMS_WinFormsClient.Models;
using AMS_WinFormsClient.Properties;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMS_WinFormsClient
{
    public partial class frm_SocketMessages_Test : DevExpress.XtraEditors.XtraForm
    {
        public bool AllowMultiInstance { get { return false; } }
        HubConnection connection;
        public const string ServerUrl1 = "https://localhost:44325/chathub";
        public const string ServerUrl2 = "https://localhost:44325/Hubs/Breaks";
        public frm_SocketMessages_Test()
        {
            InitializeComponent();

        }

        private async void connectSignalR()
        {
            connection = new HubConnectionBuilder()
            .WithUrl(ServerUrl2)
            .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            connection.On<string>("MessageFromServer", (testString) => {
                var newMessage = $"connection.On<string>(MessageFromServer, (testString): {testString}\r\n";
                SetText(newMessage);
            });

            connection.On<SignalRData>("Update", (value) => {
                var newMessage = $"Spot Booked by: {value.UserId}  --> Values:  {value.breakId} : { value.Duration } : { value.Gross?.ToString() }: { value.Net?.ToString() }\r\n";
                SetText(newMessage);
            });

            connection.On<SignalRData>("Delete", (value) => {
                var newMessage = $"Spot Deleted by: {value.UserId} --> Values: {value.breakId} : { value.Duration } : { value.Gross?.ToString() }: { value.Net?.ToString() }\r\n";
                SetText(newMessage);
            });

            try
            {
                await connection.StartAsync();
                //messagesList.Items.Add("Connection started");
                //connectButton.IsEnabled = false;
                //sendButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        delegate void SetTextCallback(string text);

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.txtInfo.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.txtInfo.Text += text;
            }
        }

        private void frm_SocketMessages_Test_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (connection.State == HubConnectionState.Connected)
            {
                connection.StopAsync();
            }
        }

        private void frm_SocketMessages_Test_Load(object sender, EventArgs e)
        {
            connectSignalR();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtInfo.Text = "";
        }

        //private async void cmdSend_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        #region snippet_InvokeAsync
        //        await connection.InvokeAsync("SendMessage","test");
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}
    }


}
