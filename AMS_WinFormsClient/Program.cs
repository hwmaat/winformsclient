﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using DevExpress.XtraEditors;
using AMS_WinFormsClient.Properties;
using AMS_WinFormsClient.AopplicationStorage;
using DevExpress.LookAndFeel;

namespace AMS_WinFormsClient
{
    static class Program
    {
        public static ApplicationStorage AppStorage;
        public static frmMain MainForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DevExpress.UserSkins.BonusSkins.Register();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            WindowsFormsSettings.ForceDirectXPaint();

            UserLookAndFeel.Default.SkinName = Settings.Default["ApplicationSkinName"].ToString();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();

            AppStorage = new ApplicationStorage();

            if (Settings.Default["Api_BaseHttp"] == null)
            {
                
                Settings.Default["Api_BaseHttp"] = @"https://localhost:44325/";
                Settings.Default["SocketPath"] = "Hubs/Breaks";
                Settings.Default.Save();

            }

            
            string apiBaseHttp = Settings.Default["Api_BaseHttp"].ToString();
            if (Login() == true)
            {
               
                MainForm = new frmMain();
                Application.Run(MainForm);
            } 


        }
        private static  bool Login()
        {
            frm_Login frmLogin = new frm_Login();
            frmLogin.ShowDialog();
            if (frmLogin.DialogResult == DialogResult.OK)
            {
                return true;
            }
            else
                return false;

        }
    }
}
