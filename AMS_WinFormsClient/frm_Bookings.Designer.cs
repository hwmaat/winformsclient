﻿namespace AMS_WinFormsClient
{
    partial class frm_Bookings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Bookings));
            this.BreaksView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdPlanning = new DevExpress.XtraGrid.GridControl();
            this.ScheduleDaysView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BookingsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabPlanning = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tabOrderBookings = new DevExpress.XtraTab.XtraTabPage();
            this.grdOrderBookings = new DevExpress.XtraGrid.GridControl();
            this.grdOrderBookingsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtOrderDescription = new DevExpress.XtraEditors.TextEdit();
            this.txtOrderPeriod = new DevExpress.XtraEditors.TextEdit();
            this.lblOrderPeriod = new DevExpress.XtraEditors.LabelControl();
            this.txtOrderProduct = new DevExpress.XtraEditors.TextEdit();
            this.lblProduct = new DevExpress.XtraEditors.LabelControl();
            this.cmbChannelsx = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblSpotGroup = new DevExpress.XtraEditors.LabelControl();
            this.cmbSpotGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.cmdLoadOrderBookings = new DevExpress.XtraEditors.SimpleButton();
            this.cmbOrders = new DevExpress.XtraEditors.LookUpEdit();
            this.lblOrders = new DevExpress.XtraEditors.LabelControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.txtDemo = new DevExpress.XtraEditors.TextEdit();
            this.lblDemo = new DevExpress.XtraEditors.LabelControl();
            this.lblCurrency = new DevExpress.XtraEditors.LabelControl();
            this.txtCurrency = new DevExpress.XtraEditors.TextEdit();
            this.lblOrderDescription = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.BreaksView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdPlanning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScheduleDaysView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BookingsView)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabPlanning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.tabOrderBookings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookingsView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbChannelsx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSpotGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOrders.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDemo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrency.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BreaksView
            // 
            this.BreaksView.GridControl = this.grdPlanning;
            this.BreaksView.Name = "BreaksView";
            this.BreaksView.OptionsDetail.AllowExpandEmptyDetails = true;
            this.BreaksView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.BreaksView_RowClick);
            // 
            // grdPlanning
            // 
            this.grdPlanning.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.BreaksView;
            gridLevelNode2.LevelTemplate = this.BookingsView;
            gridLevelNode2.RelationName = "Bookings";
            gridLevelNode1.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            gridLevelNode1.RelationName = "Breaks";
            this.grdPlanning.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grdPlanning.Location = new System.Drawing.Point(0, 0);
            this.grdPlanning.MainView = this.ScheduleDaysView;
            this.grdPlanning.Name = "grdPlanning";
            this.grdPlanning.Size = new System.Drawing.Size(1207, 432);
            this.grdPlanning.TabIndex = 0;
            this.grdPlanning.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ScheduleDaysView,
            this.BookingsView,
            this.BreaksView});
            // 
            // ScheduleDaysView
            // 
            this.ScheduleDaysView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.ScheduleDaysView.GridControl = this.grdPlanning;
            this.ScheduleDaysView.Name = "ScheduleDaysView";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            // 
            // BookingsView
            // 
            this.BookingsView.GridControl = this.grdPlanning;
            this.BookingsView.Name = "BookingsView";
            // 
            // panel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.xtraTabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1213, 500);
            this.panel2.TabIndex = 1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Images = this.imageCollection1;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabPlanning;
            this.xtraTabControl1.Size = new System.Drawing.Size(1213, 500);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPlanning,
            this.tabOrderBookings});
            // 
            // tabPlanning
            // 
            this.tabPlanning.Controls.Add(this.splitContainerControl1);
            this.tabPlanning.ImageOptions.ImageIndex = 2;
            this.tabPlanning.Name = "tabPlanning";
            this.tabPlanning.Size = new System.Drawing.Size(1207, 469);
            this.tabPlanning.Text = "Book Spots";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl3);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.grdPlanning);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1207, 469);
            this.splitContainerControl1.SplitterPosition = 32;
            this.splitContainerControl1.TabIndex = 3;
            // 
            // panelControl3
            // 
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1207, 32);
            this.panelControl3.TabIndex = 2;
            // 
            // tabOrderBookings
            // 
            this.tabOrderBookings.Controls.Add(this.grdOrderBookings);
            this.tabOrderBookings.ImageOptions.ImageIndex = 5;
            this.tabOrderBookings.Name = "tabOrderBookings";
            this.tabOrderBookings.Size = new System.Drawing.Size(1207, 469);
            this.tabOrderBookings.Text = "Bookings";
            // 
            // grdOrderBookings
            // 
            this.grdOrderBookings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdOrderBookings.Location = new System.Drawing.Point(0, 0);
            this.grdOrderBookings.MainView = this.grdOrderBookingsView;
            this.grdOrderBookings.Name = "grdOrderBookings";
            this.grdOrderBookings.Size = new System.Drawing.Size(1207, 469);
            this.grdOrderBookings.TabIndex = 0;
            this.grdOrderBookings.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdOrderBookingsView});
            // 
            // grdOrderBookingsView
            // 
            this.grdOrderBookingsView.GridControl = this.grdOrderBookings;
            this.grdOrderBookingsView.Name = "grdOrderBookingsView";
            this.grdOrderBookingsView.OptionsView.ShowFooter = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panelControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1219, 604);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panelControl1, 2);
            this.panelControl1.Controls.Add(this.lblOrderDescription);
            this.panelControl1.Controls.Add(this.txtCurrency);
            this.panelControl1.Controls.Add(this.lblCurrency);
            this.panelControl1.Controls.Add(this.lblDemo);
            this.panelControl1.Controls.Add(this.txtDemo);
            this.panelControl1.Controls.Add(this.txtOrderDescription);
            this.panelControl1.Controls.Add(this.txtOrderPeriod);
            this.panelControl1.Controls.Add(this.lblOrderPeriod);
            this.panelControl1.Controls.Add(this.txtOrderProduct);
            this.panelControl1.Controls.Add(this.lblProduct);
            this.panelControl1.Controls.Add(this.cmbChannelsx);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.cmdLoadOrderBookings);
            this.panelControl1.Controls.Add(this.cmbOrders);
            this.panelControl1.Controls.Add(this.lblOrders);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(3, 3);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1213, 92);
            this.panelControl1.TabIndex = 2;
            // 
            // txtOrderDescription
            // 
            this.txtOrderDescription.Enabled = false;
            this.txtOrderDescription.Location = new System.Drawing.Point(358, 11);
            this.txtOrderDescription.Name = "txtOrderDescription";
            this.txtOrderDescription.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtOrderDescription.Size = new System.Drawing.Size(200, 18);
            this.txtOrderDescription.TabIndex = 21;
            // 
            // txtOrderPeriod
            // 
            this.txtOrderPeriod.Enabled = false;
            this.txtOrderPeriod.Location = new System.Drawing.Point(72, 66);
            this.txtOrderPeriod.Name = "txtOrderPeriod";
            this.txtOrderPeriod.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtOrderPeriod.Size = new System.Drawing.Size(212, 18);
            this.txtOrderPeriod.TabIndex = 20;
            // 
            // lblOrderPeriod
            // 
            this.lblOrderPeriod.Location = new System.Drawing.Point(9, 70);
            this.lblOrderPeriod.Name = "lblOrderPeriod";
            this.lblOrderPeriod.Size = new System.Drawing.Size(58, 13);
            this.lblOrderPeriod.TabIndex = 19;
            this.lblOrderPeriod.Text = "OrderPeriod";
            // 
            // txtOrderProduct
            // 
            this.txtOrderProduct.Enabled = false;
            this.txtOrderProduct.Location = new System.Drawing.Point(73, 39);
            this.txtOrderProduct.Name = "txtOrderProduct";
            this.txtOrderProduct.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtOrderProduct.Size = new System.Drawing.Size(211, 18);
            this.txtOrderProduct.TabIndex = 18;
            // 
            // lblProduct
            // 
            this.lblProduct.Location = new System.Drawing.Point(9, 41);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(37, 13);
            this.lblProduct.TabIndex = 17;
            this.lblProduct.Text = "Product";
            // 
            // cmbChannelsx
            // 
            this.cmbChannelsx.Location = new System.Drawing.Point(1047, 9);
            this.cmbChannelsx.Name = "cmbChannelsx";
            this.cmbChannelsx.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbChannelsx.Properties.NullText = "";
            this.cmbChannelsx.Size = new System.Drawing.Size(157, 20);
            this.cmbChannelsx.TabIndex = 8;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lblSpotGroup);
            this.groupControl1.Controls.Add(this.cmbSpotGroup);
            this.groupControl1.Location = new System.Drawing.Point(648, 7);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(269, 82);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Booking settings";
            // 
            // lblSpotGroup
            // 
            this.lblSpotGroup.Location = new System.Drawing.Point(18, 29);
            this.lblSpotGroup.Name = "lblSpotGroup";
            this.lblSpotGroup.Size = new System.Drawing.Size(51, 13);
            this.lblSpotGroup.TabIndex = 15;
            this.lblSpotGroup.Text = "SpotGroup";
            // 
            // cmbSpotGroup
            // 
            this.cmbSpotGroup.Location = new System.Drawing.Point(76, 26);
            this.cmbSpotGroup.Name = "cmbSpotGroup";
            this.cmbSpotGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSpotGroup.Properties.NullText = "";
            this.cmbSpotGroup.Size = new System.Drawing.Size(157, 20);
            this.cmbSpotGroup.TabIndex = 14;
            // 
            // cmdLoadOrderBookings
            // 
            this.cmdLoadOrderBookings.Location = new System.Drawing.Point(926, 8);
            this.cmdLoadOrderBookings.Name = "cmdLoadOrderBookings";
            this.cmdLoadOrderBookings.Size = new System.Drawing.Size(115, 23);
            this.cmdLoadOrderBookings.TabIndex = 13;
            this.cmdLoadOrderBookings.Text = "Load OrderBookings";
            this.cmdLoadOrderBookings.Click += new System.EventHandler(this.cmdLoadOrderBookings_Click);
            // 
            // cmbOrders
            // 
            this.cmbOrders.Location = new System.Drawing.Point(72, 9);
            this.cmbOrders.Name = "cmbOrders";
            this.cmbOrders.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbOrders.Properties.NullText = "";
            this.cmbOrders.Size = new System.Drawing.Size(212, 20);
            this.cmbOrders.TabIndex = 7;
            this.cmbOrders.EditValueChanged += new System.EventHandler(this.cmbOrders_EditValueChanged);
            // 
            // lblOrders
            // 
            this.lblOrders.Location = new System.Drawing.Point(9, 11);
            this.lblOrders.Name = "lblOrders";
            this.lblOrders.Size = new System.Drawing.Size(28, 13);
            this.lblOrders.TabIndex = 6;
            this.lblOrders.Text = "Order";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "page_new.gif");
            this.imageCollection1.Images.SetKeyName(1, "page_delete.gif");
            this.imageCollection1.Images.SetKeyName(2, "calendar.gif");
            this.imageCollection1.Images.SetKeyName(3, "list_settings.gif");
            this.imageCollection1.Images.SetKeyName(4, "list_components.gif");
            this.imageCollection1.Images.SetKeyName(5, "list_links.gif");
            // 
            // txtDemo
            // 
            this.txtDemo.Enabled = false;
            this.txtDemo.Location = new System.Drawing.Point(358, 39);
            this.txtDemo.Name = "txtDemo";
            this.txtDemo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtDemo.Size = new System.Drawing.Size(200, 18);
            this.txtDemo.TabIndex = 22;
            // 
            // lblDemo
            // 
            this.lblDemo.Location = new System.Drawing.Point(290, 40);
            this.lblDemo.Name = "lblDemo";
            this.lblDemo.Size = new System.Drawing.Size(62, 13);
            this.lblDemo.TabIndex = 23;
            this.lblDemo.Text = "Demographic";
            // 
            // lblCurrency
            // 
            this.lblCurrency.Location = new System.Drawing.Point(290, 68);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(44, 13);
            this.lblCurrency.TabIndex = 24;
            this.lblCurrency.Text = "Currency";
            // 
            // txtCurrency
            // 
            this.txtCurrency.Enabled = false;
            this.txtCurrency.Location = new System.Drawing.Point(358, 68);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtCurrency.Size = new System.Drawing.Size(200, 18);
            this.txtCurrency.TabIndex = 25;
            // 
            // lblOrderDescription
            // 
            this.lblOrderDescription.Location = new System.Drawing.Point(290, 12);
            this.lblOrderDescription.Name = "lblOrderDescription";
            this.lblOrderDescription.Size = new System.Drawing.Size(53, 13);
            this.lblOrderDescription.TabIndex = 26;
            this.lblOrderDescription.Text = "Description";
            // 
            // frm_Bookings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1219, 604);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frm_Bookings";
            this.Text = "Bookings";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Bookings_FormClosing);
            this.Shown += new System.EventHandler(this.frm_Bookings_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.BreaksView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdPlanning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScheduleDaysView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BookingsView)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabPlanning.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.tabOrderBookings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookingsView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbChannelsx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSpotGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOrders.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDemo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrency.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabPlanning;
        private DevExpress.XtraGrid.GridControl grdPlanning;
        private DevExpress.XtraGrid.Views.Grid.GridView BreaksView;
        private DevExpress.XtraTab.XtraTabPage tabOrderBookings;
        private DevExpress.XtraGrid.Views.Grid.GridView BookingsView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LookUpEdit cmbChannelsx;
        private DevExpress.XtraEditors.LookUpEdit cmbOrders;
        private DevExpress.XtraEditors.LabelControl lblOrders;
        private DevExpress.XtraGrid.GridControl grdOrderBookings;
        private DevExpress.XtraGrid.Views.Grid.GridView grdOrderBookingsView;
        private DevExpress.XtraEditors.SimpleButton cmdLoadOrderBookings;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.LabelControl lblSpotGroup;
        private DevExpress.XtraEditors.LookUpEdit cmbSpotGroup;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView ScheduleDaysView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.LabelControl lblProduct;
        private DevExpress.XtraEditors.LabelControl lblOrderPeriod;
        private DevExpress.XtraEditors.TextEdit txtOrderProduct;
        private DevExpress.XtraEditors.TextEdit txtOrderPeriod;
        private DevExpress.XtraEditors.TextEdit txtOrderDescription;
        private DevExpress.XtraEditors.LabelControl lblDemo;
        private DevExpress.XtraEditors.TextEdit txtDemo;
        private DevExpress.XtraEditors.TextEdit txtCurrency;
        private DevExpress.XtraEditors.LabelControl lblCurrency;
        private DevExpress.XtraEditors.LabelControl lblOrderDescription;
    }
}