﻿namespace AMS_WinFormsClient
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.mnuFile = new DevExpress.XtraBars.BarSubItem();
            this.mnuExit = new DevExpress.XtraBars.BarButtonItem();
            this.mnuSignalRtest = new DevExpress.XtraBars.BarButtonItem();
            this.mnuSales = new DevExpress.XtraBars.BarSubItem();
            this.mnuBookings = new DevExpress.XtraBars.BarButtonItem();
            this.mnuSystem = new DevExpress.XtraBars.BarSubItem();
            this.mnuSkin = new DevExpress.XtraBars.SkinBarSubItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.toolBarMain = new DevExpress.XtraBars.Bar();
            this.mnuTest = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.sharedImageCollection1 = new DevExpress.Utils.SharedImageCollection(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1.ImageSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.toolBarMain});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.sharedImageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuFile,
            this.mnuExit,
            this.mnuSales,
            this.mnuBookings,
            this.mnuSystem,
            this.mnuSkin,
            this.mnuSignalRtest,
            this.barButtonItem1,
            this.mnuTest});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 9;
            this.barManager1.StatusBar = this.bar3;
            this.barManager1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barManager1_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSales),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSystem)});
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // mnuFile
            // 
            this.mnuFile.Caption = "&File";
            this.mnuFile.Id = 0;
            this.mnuFile.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.mnuExit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSignalRtest)});
            this.mnuFile.Name = "mnuFile";
            // 
            // mnuExit
            // 
            this.mnuExit.Caption = "E&xit";
            this.mnuExit.Id = 1;
            this.mnuExit.ImageOptions.ImageIndex = 0;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Tag = "action";
            // 
            // mnuSignalRtest
            // 
            this.mnuSignalRtest.Caption = "SignalRtest";
            this.mnuSignalRtest.Id = 6;
            this.mnuSignalRtest.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuSignalRtest.ImageOptions.Image")));
            this.mnuSignalRtest.Name = "mnuSignalRtest";
            toolTipItem6.Text = "Open message tunnel";
            superToolTip6.Items.Add(toolTipItem6);
            this.mnuSignalRtest.SuperTip = superToolTip6;
            this.mnuSignalRtest.Tag = "formfloat,frm_SocketMessages_Test";
            // 
            // mnuSales
            // 
            this.mnuSales.Caption = "&Sales";
            this.mnuSales.Id = 2;
            this.mnuSales.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuBookings)});
            this.mnuSales.Name = "mnuSales";
            // 
            // mnuBookings
            // 
            this.mnuBookings.Caption = "&Plannning";
            this.mnuBookings.Id = 3;
            this.mnuBookings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuBookings.ImageOptions.Image")));
            this.mnuBookings.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuBookings.ImageOptions.LargeImage")));
            this.mnuBookings.Name = "mnuBookings";
            toolTipItem5.Text = "Spot planning screen";
            superToolTip5.Items.Add(toolTipItem5);
            this.mnuBookings.SuperTip = superToolTip5;
            this.mnuBookings.Tag = "form,frm_Bookings";
            // 
            // mnuSystem
            // 
            this.mnuSystem.Caption = "S&ystem";
            this.mnuSystem.Id = 4;
            this.mnuSystem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSkin)});
            this.mnuSystem.Name = "mnuSystem";
            // 
            // mnuSkin
            // 
            this.mnuSkin.Caption = "Skin";
            this.mnuSkin.Id = 5;
            this.mnuSkin.Name = "mnuSkin";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // toolBarMain
            // 
            this.toolBarMain.BarName = "toolBarMain";
            this.toolBarMain.DockCol = 0;
            this.toolBarMain.DockRow = 1;
            this.toolBarMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.toolBarMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuBookings),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSignalRtest, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuTest)});
            this.toolBarMain.OptionsBar.AllowQuickCustomization = false;
            this.toolBarMain.OptionsBar.DisableClose = true;
            this.toolBarMain.OptionsBar.DisableCustomization = true;
            this.toolBarMain.OptionsBar.DrawDragBorder = false;
            this.toolBarMain.OptionsBar.UseWholeRow = true;
            this.toolBarMain.Text = "Custom 4";
            // 
            // mnuTest
            // 
            this.mnuTest.Caption = "&Test";
            this.mnuTest.Id = 8;
            this.mnuTest.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuTest.ImageOptions.Image")));
            this.mnuTest.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuTest.ImageOptions.LargeImage")));
            this.mnuTest.Name = "mnuTest";
            this.mnuTest.Tag = "form,frm_Test";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1015, 53);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 526);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1015, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 53);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 473);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1015, 53);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 473);
            // 
            // sharedImageCollection1
            // 
            // 
            // 
            // 
            this.sharedImageCollection1.ImageSource.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("sharedImageCollection1.ImageSource.ImageStream")));
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(0, "exit.png");
            this.sharedImageCollection1.ParentControl = this;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 7;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabbedMdiManager1.Appearance.Image")));
            this.xtraTabbedMdiManager1.Appearance.Options.UseImage = true;
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // frmMain
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 549);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IsMdiContainer = true;
            this.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.Name = "frmMain";
            this.Text = "AMS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1.ImageSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarSubItem mnuFile;
        private DevExpress.XtraBars.BarButtonItem mnuExit;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem mnuSales;
        private DevExpress.XtraBars.BarButtonItem mnuBookings;
        private DevExpress.XtraBars.BarSubItem mnuSystem;
        private DevExpress.XtraBars.SkinBarSubItem mnuSkin;
        private DevExpress.Utils.SharedImageCollection sharedImageCollection1;
        private DevExpress.XtraBars.BarButtonItem mnuSignalRtest;
        private DevExpress.XtraBars.Bar toolBarMain;
        private DevExpress.XtraBars.BarButtonItem mnuTest;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
    }
}