﻿namespace AMS_WinFormsClient
{
    partial class frm_Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabOrderBookings = new DevExpress.XtraTab.XtraTabPage();
            this.grdOrderBookings = new DevExpress.XtraGrid.GridControl();
            this.grdOrderBookingsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.cmdLoadOrderBookings = new DevExpress.XtraEditors.SimpleButton();
            this.lblChannel = new DevExpress.XtraEditors.LabelControl();
            this.cmbChannels = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbOrders = new DevExpress.XtraEditors.LookUpEdit();
            this.lblOrders = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabOrderBookings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookingsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbChannels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOrders.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panelControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1140, 574);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.xtraTabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 117);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1134, 454);
            this.panel2.TabIndex = 1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabOrderBookings;
            this.xtraTabControl1.Size = new System.Drawing.Size(1134, 454);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabOrderBookings});
            // 
            // tabOrderBookings
            // 
            this.tabOrderBookings.Controls.Add(this.grdOrderBookings);
            this.tabOrderBookings.Name = "tabOrderBookings";
            this.tabOrderBookings.Size = new System.Drawing.Size(1128, 426);
            this.tabOrderBookings.Text = "Bookings";
            // 
            // grdOrderBookings
            // 
            this.grdOrderBookings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdOrderBookings.Location = new System.Drawing.Point(0, 0);
            this.grdOrderBookings.MainView = this.grdOrderBookingsView;
            this.grdOrderBookings.Name = "grdOrderBookings";
            this.grdOrderBookings.Size = new System.Drawing.Size(1128, 426);
            this.grdOrderBookings.TabIndex = 0;
            this.grdOrderBookings.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdOrderBookingsView});
            // 
            // grdOrderBookingsView
            // 
            this.grdOrderBookingsView.GridControl = this.grdOrderBookings;
            this.grdOrderBookingsView.Name = "grdOrderBookingsView";
            this.grdOrderBookingsView.OptionsView.ShowFooter = true;
            // 
            // panelControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panelControl1, 2);
            this.panelControl1.Controls.Add(this.spinEdit1);
            this.panelControl1.Controls.Add(this.cmdLoadOrderBookings);
            this.panelControl1.Controls.Add(this.lblChannel);
            this.panelControl1.Controls.Add(this.cmbChannels);
            this.panelControl1.Controls.Add(this.cmbOrders);
            this.panelControl1.Controls.Add(this.lblOrders);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(3, 3);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1134, 108);
            this.panelControl1.TabIndex = 2;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(477, 36);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(100, 20);
            this.spinEdit1.TabIndex = 14;
            this.spinEdit1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.spinEdit1_EditValueChanging);
            // 
            // cmdLoadOrderBookings
            // 
            this.cmdLoadOrderBookings.Location = new System.Drawing.Point(76, 66);
            this.cmdLoadOrderBookings.Name = "cmdLoadOrderBookings";
            this.cmdLoadOrderBookings.Size = new System.Drawing.Size(115, 23);
            this.cmdLoadOrderBookings.TabIndex = 13;
            this.cmdLoadOrderBookings.Text = "Load OrderBookings";
            this.cmdLoadOrderBookings.Click += new System.EventHandler(this.cmdLoadOrderBookings_Click);
            // 
            // lblChannel
            // 
            this.lblChannel.Location = new System.Drawing.Point(14, 43);
            this.lblChannel.Name = "lblChannel";
            this.lblChannel.Size = new System.Drawing.Size(39, 13);
            this.lblChannel.TabIndex = 9;
            this.lblChannel.Text = "Channel";
            // 
            // cmbChannels
            // 
            this.cmbChannels.Location = new System.Drawing.Point(76, 40);
            this.cmbChannels.Name = "cmbChannels";
            this.cmbChannels.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbChannels.Properties.NullText = "";
            this.cmbChannels.Size = new System.Drawing.Size(261, 20);
            this.cmbChannels.TabIndex = 8;
            // 
            // cmbOrders
            // 
            this.cmbOrders.Location = new System.Drawing.Point(76, 14);
            this.cmbOrders.Name = "cmbOrders";
            this.cmbOrders.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbOrders.Properties.NullText = "";
            this.cmbOrders.Size = new System.Drawing.Size(261, 20);
            this.cmbOrders.TabIndex = 7;
            // 
            // lblOrders
            // 
            this.lblOrders.Location = new System.Drawing.Point(16, 17);
            this.lblOrders.Name = "lblOrders";
            this.lblOrders.Size = new System.Drawing.Size(28, 13);
            this.lblOrders.TabIndex = 6;
            this.lblOrders.Text = "Order";
            // 
            // frm_Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 574);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frm_Test";
            this.Text = "frm_Test";
            this.Shown += new System.EventHandler(this.frm_Test_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabOrderBookings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderBookingsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbChannels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOrders.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabOrderBookings;
        private DevExpress.XtraGrid.GridControl grdOrderBookings;
        private DevExpress.XtraGrid.Views.Grid.GridView grdOrderBookingsView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton cmdLoadOrderBookings;
        private DevExpress.XtraEditors.LabelControl lblChannel;
        private DevExpress.XtraEditors.LookUpEdit cmbChannels;
        private DevExpress.XtraEditors.LookUpEdit cmbOrders;
        private DevExpress.XtraEditors.LabelControl lblOrders;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
    }
}