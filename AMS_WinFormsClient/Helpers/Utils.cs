﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMS_WinFormsClient.Helpers
{
    public static class Utils
    {

        public static bool FormIsLoaded(string formName)
        {
            Form form_loaded = Application.OpenForms.Cast<Form>().Where(form => form.Name == formName).FirstOrDefault();

            if (form_loaded != null)
                {
                    if (form_loaded.GetType().GetProperty("AllowMultiInstance") == null)
                        return true;
                    else
                        return false;
                }
            else
                return false;
        }
    }
}
