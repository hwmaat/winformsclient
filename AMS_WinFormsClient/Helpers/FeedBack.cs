﻿using DevExpress.XtraSplashScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Diagnostics;
using System.Windows.Forms;

namespace AMS_WinFormsClient.Helpers
{

    public enum WaitFormCommand
    {
        SetProgress,
        SetMaximum,
        SetText,
        Clear,
        ShowProgress,
        HideProgress
    }
    public class FeedBack : IDisposable
    {

        public SplashScreenManager ssManager { get; set; }
        public System.Windows.Forms.Form ParentForm { get; set; }
        public string WaitText { get; set; }
        public string InstanceName { get; set; }
        public bool Enabled{get; set;}

        private Stopwatch stopWatch = new Stopwatch();
        private System.Timers.Timer tmr = new System.Timers.Timer(1000);
        public FeedBack(System.Windows.Forms.Form p)
        {
            this.Enabled = true;
            ParentForm = p;
            ssManager = new SplashScreenManager(ParentForm, typeof(WaitForm1), true, true);
            ssManager.ShowWaitForm();
        }
        public void StartWait( string waitText)
        {
            WaitText = waitText;
            tmr.Elapsed+= new ElapsedEventHandler(TimerTick);
            tmr.Enabled = true;
            stopWatch.Start();
        }
        public void ResetTimer()
        {
            stopWatch.Restart();
        }
        public void EndWait()
        {
            stopWatch.Stop();
            stopWatch.Reset();
            tmr.Enabled = false;
            ssManager.SendCommand(WaitFormCommand.SetProgress, 0);
            ssManager.CloseWaitForm();
            this.Enabled = false;
        }
        public TimeSpan GetTimeElapsed()
        {
            return stopWatch.Elapsed;
        }
        public void TimerTick(object source, ElapsedEventArgs e)
        {
            ssManager.SendCommand(WaitFormCommand.SetText, $"{WaitText}  {stopWatch.Elapsed.ToString("mm\\:ss")}");
        }

        public void UpdateProgress(int progress)
        {
            ssManager.SendCommand(WaitFormCommand.SetProgress, progress);
        }
        public void InitProgress(int max)
        {
            if (max==-1)
            {
                ssManager.SendCommand(WaitFormCommand.HideProgress, null);
            }
            else
            {
                ssManager.SendCommand(WaitFormCommand.ShowProgress, null);
                ssManager.SendCommand(WaitFormCommand.SetMaximum, max);
            }
            
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    
                    tmr.Dispose();
                }



                disposedValue = true;
            }
        }


        // ~FeedBack() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

        }
        #endregion

    }


}
