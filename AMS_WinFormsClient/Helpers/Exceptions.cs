﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Helpers
{
    public class BookingException : Exception
    {
        public BookingException(string message) 
            : base(message)
        {
           
        }
    }
}
