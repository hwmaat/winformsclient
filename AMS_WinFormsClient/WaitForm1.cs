﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AMS_WinFormsClient.Helpers;
using DevExpress.XtraWaitForm;

namespace AMS_WinFormsClient
{
    public partial class WaitForm1 : WaitForm
    {
        
        public WaitForm1()
        {
            InitializeComponent();
            this.progressPanel1.AutoHeight = true;
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            this.progressPanel1.Description = description;
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
            WaitFormCommand command = (WaitFormCommand)cmd;
            switch (command)
            {
                case WaitFormCommand.SetProgress:
                    progressBar.EditValue = arg;
                    break;
                case WaitFormCommand.SetMaximum:
                    progressBar.Properties.Maximum = Convert.ToInt32(arg);
                    break;
                case WaitFormCommand.SetText:
                   textEdit1.Visible = true;
                   textEdit1.Text = arg.ToString();
                    break;
                case WaitFormCommand.Clear:
                    textEdit1.Text = "";
                    textEdit1.Visible = false;
                    break;
                case WaitFormCommand.ShowProgress:
                    progressBar.Visible = true;
                     break;
                case WaitFormCommand.HideProgress:
                    progressBar.Visible = false;
                    break;
                default:
                    break;
            }

        }

        #endregion



    }
}