﻿using AMS_WinFormsClient.Properties;
using DevExpress.LookAndFeel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AMS_WinFormsClient.Helpers;
using DevExpress.Skins;

namespace AMS_WinFormsClient
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        public frmMain()
        {
            
            InitializeComponent();
            
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default["ApplicationSkinName"] = UserLookAndFeel.Default.SkinName;
            Settings.Default.Save();
        }

        private void barManager1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (e.Item.Tag == null) return;

            if (e.Item.Tag.ToString() == "action")
            {
                switch (e.Item.Name)
                {
                    case "mnuExit":
                        this.Close();
                        break;
                    default:
                        break;
                }
            }
            else { 
                string[] strTagType = e.Item.Tag.ToString().Split(',');
                if (strTagType.Count() < 1) return;
                if (strTagType[0] == "form")
                {
                    LoadForm(strTagType[1]);
                }
                if (strTagType[0] == "formmodal")
                {
                    LoadFormModal(strTagType[1]);
                }
                if (strTagType[0] == "formfloat")
                {
                    LoadFormFloat(strTagType[1]);
                }
        }
        }
        public Form LoadForm(string frm, int IdToOpen = 0)
        {
            if (Utils.FormIsLoaded(frm)) { return null; }
            var form = Activator.CreateInstance(Type.GetType("AMS_WinFormsClient." + frm)) as Form;
            form.MdiParent = this;
            form.Show();
            return form;
        }
        public Form LoadFormFloat(string frm, int IdToOpen = 0)
        {
            if (Utils.FormIsLoaded(frm)) { return null; }
            var form = Activator.CreateInstance(Type.GetType("AMS_WinFormsClient." + frm)) as Form;
            form.TopMost = true;
            form.Show(this);
            return form;
        }
        public Form LoadFormModal(string frm, int IdToOpen = 0)
        {
            if (Utils.FormIsLoaded(frm)) { return null; }
            var form = Activator.CreateInstance(Type.GetType("AMS_WinFormsClient." + frm)) as Form;
            form.ShowDialog(this);
            return form;
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            UserLookAndFeel.Default.SkinName = Settings.Default["ApplicationSkinName"].ToString();
            this.mnuSkin.ImageOptions.Image= SkinCollectionHelper.GetSkinIcon(UserLookAndFeel.Default.SkinName, SkinIconsSize.Small);
        }
    }
}
