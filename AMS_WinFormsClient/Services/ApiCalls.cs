﻿using AMS_WinFormsClient.Helpers;
using AMS_WinFormsClient.Models;
using AMS_WinFormsClient.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AMS_WinFormsClient.Services
{
    public class ApiCalls
    {

        public async Task<List<BookingFromApi>> GetBookingsForBreak(int BreakId)
        {
            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings");
            var queryParams = "BreakId=" + BreakId.ToString();

            string fileJsonString = null;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);

                var response = await client.GetAsync(address.ToString() + "?" + queryParams);
                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }

            string bookings = fileJsonString;
            return JsonConvert.DeserializeObject<BookingFromApi[]>(bookings).ToList<BookingFromApi>();
        }
        public async Task<List<BookingsAggregateDTO>> GetBookingsAggregate(int ChannelId, DateTimeOffset startDate, DateTimeOffset endDate, string AggrKey)
        {

            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings/aggregate");

            var queryParams = "channel=" + ChannelId.ToString();
            queryParams += "&startdatetime=" + startDate.ToString("yyyy-MM-dd");
            queryParams += "&enddatetime=" + endDate.ToString("yyyy-MM-dd");
            queryParams += "&aggregatename=" + AggrKey;
            string fileJsonString = "";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);

                var response = await client.GetAsync(address.ToString() + "?" + queryParams);

                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }
            return JsonConvert.DeserializeObject<BookingsAggregateDTO[]>(fileJsonString).ToList();
        }
        //public List<BookingFromApi> GetBookingsForBreakSync(int BreakId)
        //{
        //    Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings");
        //    var queryParams = "BreakId=" + BreakId.ToString();

        //    //string fileJsonString = null;
        //    Task<string> fileJsonString = null;
        //    string jsonString = "";
        //    using (var client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);
        //        Task<HttpResponseMessage> response = client.GetAsync(address.ToString() + "?" + queryParams);
        //        response.Wait();

        //        fileJsonString = response.Result.Content.ReadAsStringAsync();
        //        fileJsonString.Wait();

                
        //        if (fileJsonString.Status == TaskStatus.RanToCompletion)
        //        {
        //            jsonString= fileJsonString.Result;
        //        }
        //    }

        //    string bookings = jsonString;

        //    return JsonConvert.DeserializeObject<BookingFromApi[]>(bookings).ToList<BookingFromApi>();
        //}
        public async Task<List<OrderDropDownDTO>> GetOrdersForCombo()
        {

            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Orders");
            string fileJsonString = "";
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }
            return JsonConvert.DeserializeObject<OrderDropDownDTO[]>(fileJsonString).OrderBy(o => o.Description).ToList();
        }
        public async Task<OrderFromApi> GetOrder(int orderId)
        {

            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Orders/" + orderId.ToString());
            string fileJsonString = "";
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                    
                }
            }
            return JsonConvert.DeserializeObject<OrderFromApi>(fileJsonString);
        }
        public async Task<List<ChannelDropDownDTO>> GetChannelsForCombo()
        {

            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Channels");

            string fileJsonString = "";
            using (var client = new HttpClient())
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);
                var response = await client.GetAsync(address);

                 if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }
            return JsonConvert.DeserializeObject<ChannelDropDownDTO[]>(fileJsonString).OrderBy(o => o.ChannelName).ToList(); ;
        }
        public async Task<List<ScheduleDayFromApi>> GetScheduleDays(int ChannelId, DateTimeOffset startDate, DateTimeOffset endDate)
        {

            ////https://stackoverflow.com/questions/17096201/build-query-string-for-system-net-httpclient-get

            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "ScheduleDays");

            var queryParams = "channel=" + ChannelId.ToString();
            queryParams += "&startdatetime=" + startDate.ToString("yyyy-MM-dd");
            queryParams += "&enddatetime=" + endDate.ToString("yyyy-MM-dd");
            string fileJsonString = "";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);

                var response = await client.GetAsync(address.ToString() + "?" + queryParams);

                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }
            JsonSerializerSettings x = new JsonSerializerSettings();
            x.DateTimeZoneHandling = DateTimeZoneHandling.Utc;


            return JsonConvert.DeserializeObject<ScheduleDayFromApi[]>(fileJsonString, x).ToList();

        }
        public async Task<List<BookingFromApi>> GetBookingsForOrder(int orderId)
        {
            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings");

            var queryParams = "OrderId=" + orderId.ToString();

            string fileJsonString = "";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);

                var response = await client.GetAsync(address.ToString() + "?" + queryParams);

                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }

            return JsonConvert.DeserializeObject<BookingFromApi[]>(fileJsonString).ToList();
        }
        public async Task<List<BookingFromApi>> CreateBooking(BookingRequestDTO Booking)
        {

            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings");
            string fileJsonString = "";
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);
                var response = await client.PostAsJsonAsync(address, Booking);
                
                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<BookingFromApi[]>(fileJsonString).ToList();
                } else
                {
                    string errorcontent = await response.Content.ReadAsStringAsync();
                    throw new BookingException("Something went wrong while booking:" + errorcontent.ToString());
                }
                
                //TODO:2019-08-18 Ask Bob to return a meaningfull message

                //var deserializedjsonResult = JObject.Parse(fileJsonString);
                //fileJsonString = deserializedjsonResult["result"].ToString();

            }

        }
        public async Task<bool> DeleteBooking(int BookingId)
        {
            string bookingID = BookingId.ToString();
            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings/" + bookingID);

            bool result;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);
                var response = await client.DeleteAsync(address);

                result = response.IsSuccessStatusCode;
            }

            return result;
        }
    }
}