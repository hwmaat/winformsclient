﻿using AMS_WinFormsClient.Models;
using AMS_WinFormsClient.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMS_WinFormsClient
{

    public partial class frm_Login : DevExpress.XtraEditors.XtraForm
    {

        public bool loginCancelled { set; get; }
        public frm_Login()
        {
            InitializeComponent();
            string userName = "";
            string passWord = "";
            if (Settings.Default["UserName"] != null)
                userName = Settings.Default["UserName"].ToString();
            if (Settings.Default["Password"] != null)
                passWord = Settings.Default["Password"].ToString();

            this.txtUserName.Text = userName;
            this.txtPassword.Text = passWord;

        }

        private void cmdLogin_Click(object sender, EventArgs e)
        {
            if (Login())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
             
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            loginCancelled = true;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private bool Login()
        {

            //in this function we force the code to wait until the async calls are finished.
            //have to do so because we need to be authorized before we can continue
            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "User/authenticate");
            //string fileJsonString = "";

            UserCredentials uc = new UserCredentials();

            uc.id = this.txtUserName.Text;
            uc.password = this.txtPassword.Text;

           
            using (var client = new HttpClient())
            {

                Task<HttpResponseMessage> response = client.PostAsJsonAsync(address, uc);

                response.Wait();

                Task<string> fileJsonString = response.Result.Content.ReadAsStringAsync();
                fileJsonString.Wait();

                uc = JsonConvert.DeserializeObject<UserCredentials>(fileJsonString.Result);

            }

            if (uc.token != "")
            {
                Program.AppStorage.Bearer = uc.token;
                return true;
            }
            else
                return false;

        }

        private void cmdLogin_Click_1(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {

        }
    }
}


