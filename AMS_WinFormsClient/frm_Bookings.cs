﻿using AMS_WinFormsClient.Models;
using AMS_WinFormsClient.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AMS_WinFormsClient.Helpers;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using System.Drawing;
using AutoMapper;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors;
using AMS_WinFormsClient.Services;
using System.Globalization;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid;

namespace AMS_WinFormsClient
{
    public partial class frm_Bookings : DevExpress.XtraEditors.XtraForm
    {
        public bool AllowMultiInstance { get { return false; } }
        private IMapper mapper = null;
        private List<FeedBack> info = new List<FeedBack>();
        private ApiCalls apiCalls = new ApiCalls();
        private List<SimpleButton> orderChannelsButtons = new List<SimpleButton>();
        private int firstChannel = 0;


        private OrderFromApi order = null;
        public frm_Bookings()
        {
            InitializeComponent();

        }
        private async void frm_Bookings_Shown(object sender, EventArgs e)
        {
            StartWait("init");

            initMapper();

            InitBreaksGrid();
            InitOrderBookingsGrid();

            cmbOrders.Properties.BeginUpdate();

            cmbOrders.Properties.ValueMember = "OrderId";
            cmbOrders.Properties.DisplayMember = "Description";

            cmbOrders.Properties.DataSource = await apiCalls.GetOrdersForCombo();
            cmbOrders.Properties.ForceInitialize();
            cmbOrders.Properties.PopulateColumns();
            cmbOrders.Properties.Columns["OrderId"].Visible = true;
            cmbOrders.Properties.Columns["StartDate"].Visible = false;
            cmbOrders.Properties.Columns["EndDate"].Visible = false;
            cmbOrders.Properties.DropDownRows = 24;
            cmbOrders.Properties.EndUpdate();
            
            cmbOrders.EditValue = 5;

            EndWait("init");

        }
        private void FillOrderChannels(int orderId, List<OrderChannel> orderChannels)
        {
            //TODO:2019-08-19 change this into a grid so it show the columns with the target and other budget data.

            //cmbChannels.Properties.BeginUpdate();

            //cmbChannels.Properties.ValueMember = "ChannelId";
            //cmbChannels.Properties.DisplayMember = "ChannelName";

            //cmbChannels.Properties.DataSource = orderChannels;
            //cmbChannels.Properties.ForceInitialize();
            //cmbChannels.Properties.PopulateColumns();
            //cmbChannels.Properties.Columns["ChannelId"].Visible = true;
            //cmbChannels.Properties.Columns["OrderChannelId"].Visible = false;
            //cmbChannels.Properties.Columns["TargetBudget"].Visible = false;
            //cmbChannels.Properties.Columns["CostGrpGross"].Visible = false;
            //cmbChannels.Properties.Columns["CostGrpNet"].Visible = false;
            //cmbChannels.Properties.Columns["MakeGoodValue"].Visible = false;

            //cmbChannels.Properties.DropDownRows = 24;
            //cmbChannels.Properties.EndUpdate();
            int i = 0;
            firstChannel = 0;
            foreach (OrderChannel channel in orderChannels)
            {
                SimpleButton l = new SimpleButton();
                l.Text = channel.ChannelName;
                l.Tag = channel;
                l.Parent = panelControl3;
                //l.Left = 5;
                l.Width = 100; //panelControl3.Width - 10;
                l.Height = 25;
                //l.Top = i * 25;
                l.Top = 5;
                l.Left = i * 100 + 5;

                l.Click += SelectChannel;
                l.Visible = true;
                orderChannelsButtons.Append(l);
                i++;
                if (firstChannel == 0)
                    firstChannel = channel.ChannelId;
                
            }

        }
        private async void SelectChannel(object sender, EventArgs e)
        {
            SimpleButton selectedChannel = (SimpleButton)sender;
            OrderChannel channel = (OrderChannel)selectedChannel.Tag;
            int channelID = channel.ChannelId;
            await LoadGrids(channelID);
        }
        private void FillSpotGroups(int orderId, List<OrderSpotGroup> orderSpotGroups)
        {


            cmbSpotGroup.Properties.BeginUpdate();

            cmbSpotGroup.Properties.ValueMember = "OrderSpotGroupId";
            cmbSpotGroup.Properties.DisplayMember = "SpotGroupName";

            cmbSpotGroup.Properties.DataSource = orderSpotGroups;
            cmbSpotGroup.Properties.ForceInitialize();
            cmbSpotGroup.Properties.PopulateColumns();
            cmbSpotGroup.Properties.Columns["OrderSpotGroupId"].Visible = true;
            cmbSpotGroup.Properties.Columns["OrderSpotGroupId"].Width = 75;
            cmbSpotGroup.Properties.Columns["SpotGroupName"].Width = 250;
            cmbSpotGroup.Properties.Columns["SpotGroupId"].Visible = false;
            cmbSpotGroup.Properties.Columns["ApplyIndexCorrection"].Visible = false;
            cmbSpotGroup.Properties.Columns["SpotGroupIndex"].Visible = false;

            cmbSpotGroup.Properties.DropDownRows = 5;
            cmbSpotGroup.Properties.EndUpdate();
            cmbSpotGroup.EditValue = 1;


        }
        private void frm_Bookings_FormClosing(object sender, FormClosingEventArgs e)
        {
            apiCalls = null;

        }
        private async void BookSpot_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            DevExpress.XtraGrid.GridControl breaksGridControl = (DevExpress.XtraGrid.GridControl)((System.Windows.Forms.Control)sender).Parent;
            GridView activeGridView = (GridView)grdPlanning.FocusedView;
            int orderId = Convert.ToInt32(cmbOrders.EditValue);
            int breakId = Convert.ToInt32(activeGridView.GetFocusedRowCellValue("BreakId"));

            if (activeGridView.Name == "BreaksView" && e.Button.Tag.ToString() == "bookspot")
            {
                var dr = cmbSpotGroup.GetSelectedDataRow();
                if (dr == null)
                {
                    MessageBox.Show(this, "No spotgroup selected!", "Warning", MessageBoxButtons.OK);

                }
                else
                {
                    StartWait("Booking");

                    GridView scheduleDayView = (GridView)activeGridView.ParentView;

                    int orderSpotGroupId = Convert.ToInt32(cmbSpotGroup.EditValue);
                    int spotGroupId = ((OrderSpotGroup)dr).SpotGroupId;
                    List<BookingFromApi> bookingsCreated = null;
                    try
                    {
                       bookingsCreated = await apiCalls.CreateBooking(new BookingRequestDTO { OrderId = orderId, BreakId = breakId, SpotGroupId = spotGroupId, Orderspotgroupid = orderSpotGroupId });
                    }
                    catch (BookingException ex)
                    {

                        MessageBox.Show(this, ex.Message, "Error while creating booking", MessageBoxButtons.OK);
                    }
                    
                    if (bookingsCreated != null)
                    {
                        BreaksView.BeginDataUpdate();
                        activeGridView.CollapseMasterRow(activeGridView.FocusedRowHandle);
                        activeGridView.ExpandMasterRow(activeGridView.FocusedRowHandle);
                        BreaksView.EndDataUpdate();
                    }
                    else
                    {
                        XtraMessageBox.Show("Something went wrong while booking???", "Confirmation Dialog", MessageBoxButtons.OK);
                    }
                    EndWait("Booking");
                }


            }
            if (e.Button.Tag.ToString() == "deletespot")
            {
                if (XtraMessageBox.Show("Do you wish to remove this booking?", "Confirmation Dialog", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (activeGridView.Name == "BookingsView")
                    {
                        //delete the booking from this break for the selected OrderSpotGroupId

                        int bookingID = Convert.ToInt32(activeGridView.GetFocusedRowCellValue("BookingId"));
                        int orderSpotGroupId = Convert.ToInt32(activeGridView.GetFocusedRowCellValue("orderSpotGroupId"));
                        bool result = await apiCalls.DeleteBooking(bookingID);

                        if (result)
                        {

                            GridView BreaksView = (GridView)activeGridView.ParentView;
                            GridView scheduleDayView = (GridView)BreaksView.ParentView;
                            await ReloadBreak(breakId, BreaksView.SourceRowHandle);
                            BreaksView.BeginDataUpdate();
                            BreaksView.CollapseMasterRow(BreaksView.FocusedRowHandle);
                            BreaksView.ExpandMasterRow(BreaksView.FocusedRowHandle);
                            BreaksView.EndDataUpdate();
                        }
                    }
                }
            }
        }
        private void InitBreaksGrid()
        {
            #region ScheduleDays


            BandedGridView ScheduleDaysView = this.ScheduleDaysView;
            ScheduleDaysView.Columns.Clear();
            ScheduleDaysView.Bands.Clear();

            GridBand b1 = ScheduleDaysView.Bands.Add();
            GridBand orderCols = ScheduleDaysView.Bands.Add();
            orderCols.Caption = "Order values";
            orderCols.AppearanceHeader.BackColor = Color.LightSteelBlue;
            orderCols.AppearanceHeader.ForeColor = Color.DarkBlue;
            GridBand b2 = ScheduleDaysView.Bands.Add();

            GridColumn c;
            c = ScheduleDaysView.Columns.Add();
            c.Caption = "ScheduleDayId";
            c.FieldName = "ScheduleDayId";
            c.Name = "ScheduleDayId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Booked";
            c.FieldName = "HasBookingFromOrder";
            c.Name = "BookingFromOrder";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Week";
            c.FieldName = "WeekNumber";
            c.Name = "WeekNumber";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Date";
            c.FieldName = "DaystartDateTime";
            c.Name = "Date";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "ddd dd MMM yyyy";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Max-Length";
            c.FieldName = "MaxLength";
            c.Name = "MaxLength";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.BackColor = Color.Beige;
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Booked-Length";
            c.FieldName = "BookedLength";
            c.Name = "BookedLength";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.BackColor = Color.Beige;
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Available-Length";
            c.FieldName = "AvailableLength";
            c.Name = "AvailableLength";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.BackColor = Color.Beige;
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b1.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Order-Gross";
            c.FieldName = "GrossValue";
            c.Name = "GrossValue";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.SummaryItem.FieldName = "GrossValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            orderCols.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Order-Nett";
            c.FieldName = "NettValue";
            c.Name = "NettValue";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.SummaryItem.FieldName = "NettValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            orderCols.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Day-Gross";
            c.FieldName = "DayGrossValue";
            c.Name = "DayGrossValue";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "DayGrossValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b2.Columns.Add((BandedGridColumn)c);

            c = ScheduleDaysView.Columns.Add();
            c.Caption = "Day-Net";
            c.FieldName = "DayNetValue";
            c.Name = "DayNetValue";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "DayNetValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;
            b2.Columns.Add((BandedGridColumn)c);

            ScheduleDaysView.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
            GridGroupSummaryItem Gross = new GridGroupSummaryItem();
            Gross.FieldName = "GrossValue";
            Gross.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            Gross.DisplayFormat = "{0:n2}";
            Gross.ShowInGroupColumnFooter = ScheduleDaysView.Columns["GrossValue"];
            ScheduleDaysView.GroupSummary.Add(Gross);

            GridGroupSummaryItem Net = new GridGroupSummaryItem();
            Net.FieldName = "NettValue";
            Net.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            Net.DisplayFormat = "{0:n2}";
            Net.ShowInGroupColumnFooter = ScheduleDaysView.Columns["NettValue"];
            ScheduleDaysView.GroupSummary.Add(Net);

            GridGroupSummaryItem DayNet = new GridGroupSummaryItem();
            DayNet.FieldName = "DayNetValue";
            DayNet.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            DayNet.DisplayFormat = "{0:n2}";
            DayNet.ShowInGroupColumnFooter = ScheduleDaysView.Columns["DayNetValue"];
            ScheduleDaysView.GroupSummary.Add(DayNet);

            GridGroupSummaryItem DayGross = new GridGroupSummaryItem();
            DayGross.FieldName = "DayGrossValue";
            DayGross.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            DayGross.DisplayFormat = "{0:n2}";
            DayGross.ShowInGroupColumnFooter = ScheduleDaysView.Columns["DayGrossValue"];
            ScheduleDaysView.GroupSummary.Add(DayGross);

            ScheduleDaysView.OptionsView.ShowFooter = true;
            
            ScheduleDaysView.OptionsView.ColumnAutoWidth = false;
            ScheduleDaysView.OptionsDetail.AllowExpandEmptyDetails = true;
            ScheduleDaysView.OptionsBehavior.Editable = false;
            ScheduleDaysView.OptionsView.ShowIndicator = false;
            //ScheduleDaysView.OptionsView.ShowGroupPanel = false;
            ScheduleDaysView.OptionsFilter.AllowFilterEditor = true;
            ScheduleDaysView.OptionsCustomization.AllowFilter = true;
            ScheduleDaysView.OptionsSelection.EnableAppearanceFocusedRow = false;

            #endregion ScheduleDays
            #region Breaks
            GridView breaksView = this.BreaksView;
            breaksView.Columns.Clear();
            breaksView.Appearance.Row.BackColor = Color.Beige;
            breaksView.Appearance.Row.Options.UseBackColor = true;
            breaksView.OptionsView.ShowFooter = true;

            RepositoryItemButtonEdit btnBooking = new RepositoryItemButtonEdit();
            btnBooking.TextEditStyle = TextEditStyles.HideTextEditor;
            btnBooking.ButtonClick += BookSpot_ButtonClick;

            btnBooking.Buttons[0].Kind = ButtonPredefines.Glyph;
            btnBooking.Buttons[0].ImageOptions.ImageList = this.imageCollection1;
            btnBooking.Buttons[0].ImageOptions.ImageIndex = 0;
            btnBooking.Buttons[0].ImageOptions.EnableTransparency = true;
            btnBooking.Buttons[0].Tag = "bookspot";
            btnBooking.Buttons[0].Caption = "Create Booking";

            //var btn = new DevExpress.XtraEditors.Controls.EditorButton();
            //btn.Kind = ButtonPredefines.Glyph;
            //btn.ImageOptions.ImageList = this.imageCollection1;
            //btn.ImageOptions.ImageIndex = 1;
            //btn.ImageOptions.EnableTransparency = true;
            //btn.Tag = "deletespot";
            //btn.Caption = "Delete Booking";

            //btnBooking.Buttons.Add(btn);

            c = breaksView.Columns.Add();
            c.Caption = "action";
            c.FieldName = "";
            c.Name = "BookSpot";
            c.ColumnEdit = btnBooking;
            c.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.AppearanceCell.BackColor = Color.LightGray;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "BreakId";
            c.FieldName = "BreakId";
            c.Name = "breakId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "StartTime";
            c.FieldName = "BreakStartTime";
            c.Name = "breakStartTime";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "HH:mm";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Max-Length";
            c.FieldName = "MaxLength";
            c.Name = "MaxLength";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.BackColor = Color.LightGray;
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Booked-Length";
            c.FieldName = "BookedLength";
            c.Name = "BookedLength";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.BackColor = Color.LightGray;
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Avail-Length";
            c.FieldName = "AvailableLength";
            c.Name = "AvailableLength";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.BackColor = Color.LightGray;
            c.AppearanceCell.ForeColor = Color.DarkBlue;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "#Spots";
            c.FieldName = "SpotCount";
            c.Name = "SpotCount";
            c.Width = 100;
            c.SummaryItem.FieldName = "SpotCount";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n0}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Order Gross";
            c.FieldName = "GrossValue";
            c.Name = "GrossValue";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "GrossValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Order Net";
            c.FieldName = "NetValue";
            c.Name = "NetValue";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "NetValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Break Gross";
            c.FieldName = "BreakGrossValue";
            c.Name = "BreakGrossValue";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "BreakGrossValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Break Net";
            c.FieldName = "BreakNetValue";
            c.Name = "BreakNetValue";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "BreakNetValue";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = breaksView.Columns.Add();
            c.Caption = "Booked";
            c.FieldName = "HasBookingFromOrder";
            c.Name = "BookingFromOrder";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;



            breaksView.OptionsView.ColumnAutoWidth = false;
            breaksView.OptionsDetail.AllowExpandEmptyDetails = true;
            breaksView.OptionsBehavior.Editable = true;
            //breaksView.OptionsView.ShowIndicator = false;
            breaksView.OptionsView.ShowGroupPanel = false;
            breaksView.SynchronizeClones = true;
            breaksView.OptionsSelection.EnableAppearanceFocusedRow = false;
            breaksView.DataController.AllowIEnumerableDetails = true;


            #endregion Breaks
            #region Bookings
            GridView bookingsView = this.BookingsView;
            BookingsView.Columns.Clear();
            bookingsView.Appearance.Row.BackColor = Color.Gray;
            bookingsView.Appearance.Row.ForeColor = Color.White;
            bookingsView.Appearance.Row.Options.UseBackColor = true;
            bookingsView.Appearance.Row.Options.UseForeColor = true;
            bookingsView.OptionsView.ShowFooter = true;

            RepositoryItemButtonEdit btnDeleteSpot = new RepositoryItemButtonEdit();
            btnDeleteSpot.TextEditStyle = TextEditStyles.HideTextEditor;
            btnDeleteSpot.ButtonClick += BookSpot_ButtonClick;
            //(sender, e) => {
            //    if (e.Button.Kind == ButtonPredefines.Delete)
            //    {
            //        if (XtraMessageBox.Show("Do you wish to remove this booking?", "Confirmation Dialog", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //        {
            //            //gridView.DeleteRow(gridView.FocusedRowHandle);
            //        }
            //    }
            //};
            btnDeleteSpot.Buttons[0].Kind = ButtonPredefines.Delete;
            btnDeleteSpot.Buttons[0].Caption = "Delete Spot";
            btnDeleteSpot.Buttons[0].Tag = "deletespot";
            btnDeleteSpot.Buttons[0].ToolTip = "Delete Booking";

            c = bookingsView.Columns.Add();
            c.Caption = "action";
            c.FieldName = "";
            c.Name = "DelSpot";
            c.ColumnEdit = btnDeleteSpot;
            c.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.AppearanceCell.BackColor = Color.LightGray;

            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "BookingId";
            c.FieldName = "BookingId";
            c.Name = "BookingId";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "BreakId";
            c.FieldName = "BreakId";
            c.Name = "BreakId";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "OrderId";
            c.FieldName = "OrderId";
            c.Name = "OrderId";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "OrderDescription";
            c.FieldName = "OrderDescription";
            c.Name = "OrderDescription";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;
            
            c = bookingsView.Columns.Add();
            c.Caption = "SpotGroupId";
            c.FieldName = "orderSpotGroupId";
            c.Name = "orderSpotGroupId";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "Position";
            c.FieldName = "positioninBreak";
            c.Name = "Position";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "Pref";
            c.FieldName = "isPreferredPosition";
            c.Name = "Pref";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "Spotlength";
            c.FieldName = "Duration";
            c.Name = "Spotlength";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.ForeColor = Color.Blue;
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "basePrice";
            c.FieldName = "BasePrice";
            c.Name = "basePrice";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "BasePrice";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "startPrice";
            c.FieldName = "StartPrice";
            c.Name = "StartPrice";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "StartPrice";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;


            c = bookingsView.Columns.Add();
            c.Caption = "Gross";
            c.FieldName = "Gross";
            c.Name = "Gross";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Gross";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "Surcharge";
            c.FieldName = "Surcharge";
            c.Name = "Surcharge";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Surcharge";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "Discount";
            c.FieldName = "Discount";
            c.Name = "Discount";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Discount";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "Net";
            c.FieldName = "Net";
            c.Name = "Net";
            c.Width = 100;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Net";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.OptionsColumn.AllowEdit = false;
            c.Visible = true;

            c = bookingsView.Columns.Add();
            c.Caption = "FC";
            c.FieldName = "RatingsForecast";
            c.Name = "RatingsForecast";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "{0:n1}";
            //c.AppearanceCell.BackColor = Color.Honeydew;
            //c.AppearanceCell.ForeColor = Color.Black;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            //c = bookingsView.Columns.Add();
            //c.Caption = "BookingType";
            //c.FieldName = "BookingType";
            //c.Name = "BookingType";
            //c.Width = 50;
            //c.OptionsColumn.ReadOnly = true;
            //c.OptionsColumn.AllowEdit = false;
            //c.Visible = true;

            //c = bookingsView.Columns.Add();
            //c.Caption = "Makegood";
            //c.FieldName = "MakeGood";
            //c.Name = "MakeGood";
            //c.Width = 50;
            //c.OptionsColumn.ReadOnly = true;
            //c.OptionsColumn.AllowEdit = false;
            //c.Visible = true;

            bookingsView.OptionsDetail.AllowExpandEmptyDetails = true;
            bookingsView.OptionsView.ColumnAutoWidth = false;
            bookingsView.OptionsBehavior.Editable = true;
            bookingsView.OptionsView.ShowIndicator = false;
            bookingsView.OptionsView.ShowGroupPanel = false;
            bookingsView.SynchronizeClones = true;
            bookingsView.OptionsSelection.EnableAppearanceFocusedRow = false;
            #endregion Bookings


            ScheduleDaysView.MasterRowEmpty += grdBookingsView_MasterRowEmpty;
            ScheduleDaysView.RowCellStyle += ScheduleDaysView_RowCellStyle;

            BreaksView.MasterRowGetChildList += grdBookingsView_MasterRowGetChildListAsync;
            BreaksView.MasterRowEmpty += grdBookingsView_MasterRowEmpty;
            BreaksView.MasterRowGetRelationCount += BreaksView_MasterRowGetRelationCount;
            BreaksView.MasterRowGetRelationName += BreaksView_MasterRowGetRelationName;
            BreaksView.RowCellStyle += BreaksView_RowCellStyle;

        }
        private void BreaksView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            GridView breaksView = (GridView)sender;

            bool hasBookingFromOrder = Convert.ToBoolean(breaksView.GetRowCellValue(e.RowHandle, "HasBookingFromOrder"));
            if (hasBookingFromOrder)
            {

                switch (e.Column.Name)
                {
                    case string col when (col == "MaxLength" || col == "BookedLength" || col == "AvailableLength"):
                        //e.Appearance.BackColor = Color.LightSteelBlue;
                        //e.Appearance.ForeColor = Color.DarkBlue;
                        break;
                    case string col when (col == "GrossValue" || col == "NetValue"):
                        e.Appearance.BackColor = Color.LightSteelBlue;
                        e.Appearance.ForeColor = Color.DarkBlue;
                        //e.Appearance.Font = new Font(e.Appearance.Font.Name, e.Appearance.Font.Size, FontStyle.Bold);

                        break;
                    default:
                        //e.Appearance.BackColor = Color.LightSteelBlue;
                        //e.Appearance.ForeColor = Color.White;
                        break;
                }

            }
        }
        private void ScheduleDaysView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            GridView breaksView = (GridView)sender;

            bool hasBookingFromOrder = Convert.ToBoolean(breaksView.GetRowCellValue(e.RowHandle, "HasBookingFromOrder"));
            if (hasBookingFromOrder)
            {
                switch (e.Column.Name)
                {
                    case string col when (col == "MaxLength" || col == "BookedLength" || col == "AvailableLength"):
                        //e.Appearance.BackColor = Color.LightSteelBlue;
                        //e.Appearance.ForeColor = Color.DarkBlue;
                        break;
                    case string col when (col == "GrossValue" || col == "NettValue"):
                        e.Appearance.BackColor = Color.LightSteelBlue;
                        e.Appearance.ForeColor = Color.DarkBlue;
                        //e.Appearance.Font = new Font(e.Appearance.Font.Name, e.Appearance.Font.Size, FontStyle.Bold);

                        break;
                    default:
                        //e.Appearance.BackColor = Color.LightSteelBlue;
                        //e.Appearance.ForeColor = Color.White;
                        break;
                }

            }
        }
        private void InitOrderBookingsGrid()
        {
            GridView orderBookingsView = this.grdOrderBookingsView;
            orderBookingsView.Columns.Clear();
            GridColumn c;
            c = orderBookingsView.Columns.Add();
            c.Caption = "BookingId";
            c.FieldName = "BookingId";
            c.Name = "BookingId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "OrderId";
            c.FieldName = "OrderId";
            c.Name = "OrderId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "ChannelName";
            c.FieldName = "ChannelName";
            c.Name = "ChannelName";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "BreakId";
            c.FieldName = "BreakId";
            c.Name = "BreakId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Week";
            c.FieldName = "WeekNumber";
            c.Name = "WeekNumber";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Date";
            c.FieldName = "BreakDateTime";
            c.Name = "BreakDate";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "dd/MM/yy";
            c.OptionsColumn.ReadOnly = true;

            c.Visible = true;
            c = orderBookingsView.Columns.Add();
            c.Caption = "Time";
            c.FieldName = "BreakDateTime";
            c.Name = "BreakTime";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "HH:mm";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Pos";
            c.FieldName = "PositioninBreak";
            c.Name = "PositioninBreak";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Pref";
            c.FieldName = "IsPreferredPosition";
            c.Name = "IsPreferredPosition";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "OrderSpotGroupId";
            c.FieldName = "orderSpotGroupId";
            c.Name = "OrderSpotGroupId";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "SpotLength";
            c.FieldName = "Duration";
            c.Name = "SpotLength";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "t";
            c.AppearanceCell.ForeColor = Color.Blue;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "BasePrice";
            c.FieldName = "BasePrice";
            c.Name = "BasePrice";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "BasePrice";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "StartPrice";
            c.FieldName = "StartPrice";
            c.Name = "StartPrice";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "StartPrice";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Gross";
            c.FieldName = "Gross";
            c.Name = "Gross";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Gross";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Surcharge";
            c.FieldName = "Surcharge";
            c.Name = "Surcharge";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Surcharge";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Discount";
            c.FieldName = "Discount";
            c.Name = "Discount";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Discount";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Net";
            c.FieldName = "Net";
            c.Name = "Net";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "Net";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n2}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "FC";
            c.FieldName = "RatingsForecast";
            c.Name = "RatingsForecast";
            c.Width = 75;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.SummaryItem.FieldName = "RatingsForecast";
            c.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            c.SummaryItem.DisplayFormat = "{0:n1}";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;


            orderBookingsView.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
            GridGroupSummaryItem BasePrice = new GridGroupSummaryItem();
            BasePrice.FieldName = "BasePrice";
            BasePrice.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            BasePrice.DisplayFormat = "{0:n2}";
            BasePrice.ShowInGroupColumnFooter = orderBookingsView.Columns["BasePrice"];
            orderBookingsView.GroupSummary.Add(BasePrice);

            GridGroupSummaryItem StartPrice = new GridGroupSummaryItem();
            StartPrice.FieldName = "StartPrice";
            StartPrice.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            StartPrice.DisplayFormat = "{0:n2}";
            StartPrice.ShowInGroupColumnFooter = orderBookingsView.Columns["StartPrice"];
            orderBookingsView.GroupSummary.Add(StartPrice);

            GridGroupSummaryItem Gross = new GridGroupSummaryItem();
            Gross.FieldName = "Gross";
            Gross.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            Gross.DisplayFormat = "{0:n2}";
            Gross.ShowInGroupColumnFooter = orderBookingsView.Columns["Gross"];
            orderBookingsView.GroupSummary.Add(Gross);

            GridGroupSummaryItem Surcharge = new GridGroupSummaryItem();
            Surcharge.FieldName = "Surcharge";
            Surcharge.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            Surcharge.DisplayFormat = "{0:n2}";
            Surcharge.ShowInGroupColumnFooter = orderBookingsView.Columns["Surcharge"];
            orderBookingsView.GroupSummary.Add(Surcharge);

            GridGroupSummaryItem Discount = new GridGroupSummaryItem();
            Discount.FieldName = "Discount";
            Discount.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            Discount.DisplayFormat = "{0:n2}";
            Discount.ShowInGroupColumnFooter = orderBookingsView.Columns["Discount"];
            orderBookingsView.GroupSummary.Add(Discount);

            GridGroupSummaryItem Net = new GridGroupSummaryItem();
            Net.FieldName = "Net";
            Net.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            Net.DisplayFormat = "{0:n2}";
            Net.ShowInGroupColumnFooter = orderBookingsView.Columns["Net"];
            orderBookingsView.GroupSummary.Add(Net);

            GridGroupSummaryItem RatingsForecast = new GridGroupSummaryItem();
            RatingsForecast.FieldName = "RatingsForecast";
            RatingsForecast.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            RatingsForecast.DisplayFormat = "{0:n2}";
            RatingsForecast.ShowInGroupColumnFooter = orderBookingsView.Columns["RatingsForecast"];
            orderBookingsView.GroupSummary.Add(RatingsForecast);


            orderBookingsView.OptionsView.ColumnAutoWidth = false;
            orderBookingsView.OptionsDetail.AllowExpandEmptyDetails = true;
            orderBookingsView.OptionsBehavior.Editable = false;
            orderBookingsView.OptionsView.ShowIndicator = true;
            orderBookingsView.OptionsView.ShowGroupPanel = true;
            orderBookingsView.OptionsFilter.AllowFilterEditor = true;
            orderBookingsView.OptionsCustomization.AllowFilter = true;
            orderBookingsView.OptionsView.ShowFooter = true;
        }
        private void initMapper()
        {
            MapperConfiguration mapperconfig = new MapperConfiguration(cfg =>
            {


                var scheduleDayMap = cfg.CreateMap<ScheduleDayFromApi, ScheduleDayDTO>();
                scheduleDayMap.ForMember(m => m.GrossValue, opts => opts.MapFrom(_ => 0));
                scheduleDayMap.ForMember(m => m.WeekNumber, opts => opts.MapFrom(s => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(s.DaystartDateTime.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)));
                scheduleDayMap.ForMember(m => m.NettValue, opts => opts.MapFrom(_ => 0));
                scheduleDayMap.ForMember(m => m.DayGrossValue, opts => opts.MapFrom(_ => 0));
                scheduleDayMap.ForMember(m => m.DayNetValue, opts => opts.MapFrom(_ => 0));
                scheduleDayMap.ForMember(m => m.MaxLength, opts => opts.MapFrom(_ => TimeSpan.FromSeconds(0)));
                scheduleDayMap.ForMember(m => m.BookedLength, opts => opts.MapFrom(_ => TimeSpan.FromSeconds(0)));
                scheduleDayMap.ForMember(m => m.AvailableLength, opts => opts.MapFrom(_ => TimeSpan.FromSeconds(0)));
                scheduleDayMap.ForMember(m => m.HasBookingFromOrder, opts => opts.MapFrom(_ => false));
                //zz.AfterMap((api, dto) =>
                //{
                //    Double GrossValue = GetRandomDouble(250.0, 5000.0);
                //    dto.GrossValue = GrossValue;
                //    dto.NettValue = GetRandomDouble(100.0, GrossValue);
                //    int secs = GetRandomInt(0, 480);
                //    //dto.MaxLength = TimeSpan.FromSeconds(0);
                //    //dto.BookedLength = TimeSpan.FromSeconds(0);
                //    //dto.AvailableLength = TimeSpan.FromSeconds(0);
                //});

                var breakMap = cfg.CreateMap<BreakFromApi, BreakDto>();
                breakMap.ForMember(destination => destination.BreakStartTime, opts => opts.MapFrom(_ => new DateTimeOffset()));

                breakMap.ForMember(destination => destination.MaxLength, opts => opts.MapFrom(x => TimeSpan.FromMilliseconds(x.MaxLength)));
                breakMap.ForMember(destination => destination.BookedLength, opts => opts.MapFrom(x => TimeSpan.FromMilliseconds(x.MaxLength - x.BookedLength)));
                breakMap.ForMember(destination => destination.AvailableLength, opts => opts.MapFrom(x => TimeSpan.FromMilliseconds(x.BookedLength)));
                breakMap.ForMember(destination => destination.SpotCount, opts => opts.MapFrom(_ => 0));
                breakMap.ForMember(destination => destination.GrossValue, opts => opts.MapFrom(_ => 0));
                breakMap.ForMember(destination => destination.NetValue, opts => opts.MapFrom(_ => 0));
                breakMap.ForMember(destination => destination.BreakGrossValue, opts => opts.MapFrom(_ => 0));
                breakMap.ForMember(destination => destination.BreakNetValue, opts => opts.MapFrom(_ => 0));
                breakMap.ForMember(destination => destination.HasBookingFromOrder, opts => opts.MapFrom(_ => false));

                //zx.AfterMap((api, dto) =>
                //{
                //    dto.MaxLength = TimeSpan.FromSeconds(300);
                //    dto.AvailableLength = dto.MaxLength - dto.BookedLength;
                //});

                var bookingMap = cfg.CreateMap<BookingFromApi, BookingDTO>();
                bookingMap.ForMember(destination => destination.Duration, opts => opts.MapFrom(x => TimeSpan.FromMilliseconds(x.Duration)));
                bookingMap.ForMember(gv => gv.WeekNumber, opts => opts.MapFrom(s => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(s.BreakDateTime.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday)));
                bookingMap.ForMember(gv => gv.BasePrice, opts => opts.MapFrom(x => x.BasePrice));
                bookingMap.ForMember(gv => gv.StartPrice, opts => opts.MapFrom(x => x.StartPrice));
                bookingMap.ForMember(gv => gv.Gross, opts => opts.MapFrom(x => x.Gross));
                bookingMap.ForMember(gv => gv.Surcharge, opts => opts.MapFrom(x => x.Surcharge));
                bookingMap.ForMember(gv => gv.Discount, opts => opts.MapFrom(x => x.Discount));
                bookingMap.ForMember(gv => gv.Net, opts => opts.MapFrom(x => x.Net));


                //var orderBookingMap = cfg.CreateMap<Book, BookingsDTO>();
                //orderBookingMap.ForMember(destination => destination.Duration, opts => opts.MapFrom(x => TimeSpan.FromMilliseconds(x.Duration)));
            });
            mapperconfig.AssertConfigurationIsValid();
            mapper = mapperconfig.CreateMapper();
        }
        private async Task LoadBreaksGrid(int orderId, int channelId, DateTimeOffset startDate, DateTimeOffset endDate)
        {
            Console.WriteLine($"==> LoadBreaksGrid start");
            StartWait("scheduledays");
            grdPlanning.DataSource = null;

            List<ScheduleDayFromApi> l = await apiCalls.GetScheduleDays(channelId, startDate, endDate);

            //TODO:2019-08-15 Add scheduledayId to bookings so we can aggregate per day as well
            //Aggregate the bookings per break for the planninggrid channel period
            List<BookingsAggregateDTO> breakTotals = await apiCalls.GetBookingsAggregate(channelId, startDate, endDate, "BreakId");
            BindingList<ScheduleDayDTO> lDto = mapper.Map<List<ScheduleDayFromApi>, BindingList<ScheduleDayDTO>>(l, opts => opts.AfterMap((scheduleDayApi, scheduleDaysDto) =>
            {
                foreach (ScheduleDayDTO sDto in scheduleDaysDto)
                {
                    //travel through every break in the day and get the totals from the aggregate
                    foreach (BreakDto brk in sDto.Breaks)
                    {
                        brk.BreakStartTime = sDto.DaystartDateTime.AddMilliseconds(brk.Offset);

                        //Get the total values for this break from the aggregate

                        //TODO:2019-08-15 Add totalseconds to aggregate
                        BookingsAggregateDTO breakTotal = breakTotals.SingleOrDefault(brkT => brkT.Key.ToString() == brk.BreakId.ToString());
                        if (breakTotal != null)
                        {
                            brk.GrossValue = 0;
                            brk.NetValue = 0;
                            brk.BreakGrossValue = (Double)breakTotal.TotalGross;
                            brk.BreakNetValue = (Double)breakTotal.TotalNet;
                            brk.SpotCount = (int)breakTotal.Count;
                        }
                        else
                        {
                            brk.NetValue = 0;
                            brk.SpotCount = 0;
                            brk.SpotCount = 0;
                            brk.BreakGrossValue = 0;
                            brk.BreakNetValue = 0;
                        }
                    }

                    //now get the total for the day level. This can be achieved by summing all the values from the breaks
                    //belonging to this day for which we just calculated the break totals from the aggregate

                    //we cannot set the totals for the selected order because there is no option to apply a filter
                    //on break level
                    sDto.GrossValue = 0;
                    sDto.NettValue = 0;
                    sDto.DayGrossValue = sDto.Breaks.Sum(b => b.BreakGrossValue);
                    sDto.DayNetValue = sDto.Breaks.Sum(b => b.BreakNetValue);
                    sDto.MaxLength = TimeSpan.FromMilliseconds(sDto.Breaks.Sum(b => b.MaxLength.TotalMilliseconds));
                    sDto.BookedLength = TimeSpan.FromMilliseconds(sDto.Breaks.Sum(b => b.BookedLength.TotalMilliseconds));
                    sDto.AvailableLength = TimeSpan.FromMilliseconds(sDto.Breaks.Sum(b => b.AvailableLength.TotalMilliseconds));
                }
            }));


            grdPlanning.BeginUpdate();
            grdPlanning.DataSource = lDto; // scheduleDays;
            grdPlanning.EndUpdate();
            EndWait("scheduledays");

            Console.WriteLine($"==> LoadBreaksGrid end");
        }
        private async Task LoadOrderBookingsGrid(int orderId)
        {
            Console.WriteLine($"==> LoadOrderBookingsGrid start");
            StartWait("order bookings");
            grdOrderBookings.DataSource = null;

            List<BookingFromApi> orderBookings = await apiCalls.GetBookingsForOrder(orderId);

            BindingList<BookingDTO> orderBookingsDto = mapper.Map<List<BookingFromApi>, BindingList<BookingDTO>>(orderBookings);


            grdOrderBookings.BeginUpdate();
            grdOrderBookings.DataSource = orderBookingsDto;
            grdOrderBookings.EndUpdate();

            EndWait("order bookings");
            Console.WriteLine($"====> FindOrderBookings(); start");
            FindOrderBookings();
            Console.WriteLine($"====> FindOrderBookings(); start");

            Console.WriteLine($"==> LoadOrderBookingsGrid end");
        }
        private void StartWait(string forItem)
        {
            FeedBack fb = new FeedBack(this);
            fb.InitProgress(-1);
            fb.StartWait("Querying API ");
            fb.InstanceName = forItem;
            info.Add(fb);
        }
        private void EndWait(string forItem)
        {
            int infoIndex = info.FindIndex(f => f.InstanceName == forItem);
            info[infoIndex].EndWait();
            info[infoIndex].Dispose();
            info.RemoveAt(infoIndex);
        }
        private async Task<BindingList<BookingDTO>> ReloadBreak(int breakID, int DayrowHandle)
        {
            //1. reloads the bookings from the API to recalculate the totals
            List<BookingFromApi> bookings = await apiCalls.GetBookingsForBreak(breakID);
            BindingList<BookingDTO> bookingsDto = null;
            int orderId = Convert.ToInt32(cmbOrders.EditValue);

            if (bookings != null)
            {
                bookingsDto = mapper.Map<List<BookingFromApi>, BindingList<BookingDTO>>(bookings);

                //2. find the schedule row in the grid with breakId 

                BindingList<ScheduleDayDTO> lDto = (BindingList<ScheduleDayDTO>)grdPlanning.DataSource;

                //There can only be one day with breakId == breakId, so get the day that has breakId == breakID
                ScheduleDayDTO searchDayWithBreak = lDto.Where(s => s.Breaks.Any(b => b.BreakId == breakID)).FirstOrDefault();

                if (DayrowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                {
                    //3. get the break in the Breaks Collection
                    BindingList<BreakDto> breaks = searchDayWithBreak.Breaks;
                    BreakDto brk = breaks.FirstOrDefault(b => b.BreakId == breakID);
                    if (brk != null)
                    {
                        //4. If found, aggregate the bookings values to the breaks level

                        brk.GrossValue = (double)bookingsDto.Where(b => b.OrderId == orderId).Sum(f => f.Gross);
                        brk.NetValue = (double)bookingsDto.Where(b => b.OrderId == orderId).Sum(f => f.Net);
                        brk.BreakGrossValue = (double)bookingsDto.Sum(f => f.Gross);
                        brk.BreakNetValue = (double)bookingsDto.Sum(f => f.Net);
                        brk.SpotCount = bookingsDto.Count;
                        brk.BookedLength = TimeSpan.FromMilliseconds(bookingsDto.Sum(f => f.Duration.TotalMilliseconds));
                        brk.AvailableLength = brk.MaxLength - brk.BookedLength;

                        brk.HasBookingFromOrder = (bookings.Find(b => b.OrderId == orderId) != null);

                        int dayHasBreakBooked = searchDayWithBreak.Breaks.Where(s => s.HasBookingFromOrder == true).Count();
                        searchDayWithBreak.HasBookingFromOrder = (dayHasBreakBooked > 0);
                        searchDayWithBreak.DayGrossValue = (double)searchDayWithBreak.Breaks.Sum(s => s.BreakGrossValue);
                        searchDayWithBreak.DayNetValue = (double)searchDayWithBreak.Breaks.Sum(s => s.BreakNetValue);

                        searchDayWithBreak.GrossValue = (double)searchDayWithBreak.Breaks.Sum(s => s.GrossValue);
                        searchDayWithBreak.NettValue = (double)searchDayWithBreak.Breaks.Sum(s => s.NetValue);

                        searchDayWithBreak.BookedLength = TimeSpan.FromMilliseconds(searchDayWithBreak.Breaks.Sum(s => s.BookedLength.TotalMilliseconds));
                        searchDayWithBreak.AvailableLength = searchDayWithBreak.MaxLength - searchDayWithBreak.BookedLength;

                    }
                }
            }
            return bookingsDto;
        }
        private async Task LoadGrids(int channelID)
        {

            var dr = cmbOrders.GetSelectedDataRow();
            DateTimeOffset startDate = ((AMS_WinFormsClient.Models.OrderDropDownDTO)dr).StartDate;
            DateTimeOffset endDate = ((AMS_WinFormsClient.Models.OrderDropDownDTO)dr).EndDate;
            
            int orderId = Convert.ToInt32(cmbOrders.EditValue);

            await LoadBreaksGrid(orderId, channelID, startDate, endDate);

            await LoadOrderBookingsGrid(orderId);


        }
        private void FindOrderBookings()
        {
            //travels through the bookings from the order, searches the scheduleday AND the breakID in the planning grid
            //If found, updates the fields   public bool BookingFromOrder { get; set; } in both the breakDTo and the ScheduleDto to reflect
            //there is a booking from that order. 
            //That field is then used to update the coloring etc of the row
            
            BindingList<ScheduleDayDTO> scheduleDays = (BindingList<ScheduleDayDTO>)grdPlanning.DataSource;
            BindingList<BookingDTO> orderBookings = (BindingList<BookingDTO>)grdOrderBookings.DataSource;

            Console.WriteLine($"FindOrderBookings scheduleDays == null: {scheduleDays == null}  orderBookings == null: {orderBookings == null} ");

            if (orderBookings!=null && scheduleDays!=null)
            {
                foreach (BookingDTO orderBooking in orderBookings)
                {
                    //look for the break in the scheduleDays
                    ScheduleDayDTO searchDayWithBreak = scheduleDays.Where(s => s.Breaks.Any(b => b.BreakId == orderBooking.BreakId)).FirstOrDefault();
                    if (searchDayWithBreak != null)
                    {
                        searchDayWithBreak.HasBookingFromOrder = true;
                        //now find the break
                        BindingList<BreakDto> breaks = searchDayWithBreak.Breaks;
                        //BreakDto brk = breaks.Find(b => b.BreakId == orderBooking.BreakId);
                        BreakDto brk = breaks.FirstOrDefault(b => b.BreakId == orderBooking.BreakId);
                        if (brk != null)
                        {
                            brk.GrossValue = (double)orderBookings.Where(w=>w.BreakId == brk.BreakId).Sum(b => b.Gross);
                            brk.NetValue = (double)orderBookings.Where(w => w.BreakId == brk.BreakId).Sum(b => b.Net);
                            brk.HasBookingFromOrder = true;
                        }
                        
                    }

                }

                foreach (ScheduleDayDTO scheduleDay in scheduleDays)
                {
                    scheduleDay.GrossValue = 0;

                    var x = from bookings in orderBookings
                            from breaks  in scheduleDay.Breaks
                            where breaks.BreakId == bookings.BreakId
                            select bookings;



                    scheduleDay.GrossValue = (double)x.Sum(g => g.Gross);
                    scheduleDay.NettValue = (double)x.Sum(g => g.Net);

                    //eventsdb.Where(e => eventids.Contains(e));


                    //scheduleDay.NettValue = 0;
                }

            } 
                


        }
        private async void cmdLoadOrderBookings_Click(object sender, EventArgs e)
        {
            int orderId = Convert.ToInt32(cmbOrders.EditValue);
            await LoadOrderBookingsGrid(orderId);
            //FindOrderBookings();
        }
        //private async void grdBookingsView_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        //{
        //    GridView activeGridview = (GridView)sender;
        //    if (activeGridview.LevelName == "Breaks")
        //    {

        //        int breakId = Convert.ToInt32(activeGridview.GetRowCellValue(e.RowHandle, "BreakId"));
        //        GridView breaksView = sender as GridView;


        //        e.ChildList = GetBookingListSync(breakId);
        //        int dayRowHandle = ((GridView)breaksView.ParentView).FocusedRowHandle;

        //        await ReloadBreak(breakId, dayRowHandle);

        //    }
        //    if (activeGridview.LevelName == "")
        //    {
        //        int scheduleDayId = Convert.ToInt32(activeGridview.GetRowCellValue(e.RowHandle, "ScheduleDayId"));
        //    }
        //}
        //private BindingList<BookingDTO> GetBookingListSync(int breakId)
        //{

        //    List<BookingFromApi> bookings = apiCalls.GetBookingsForBreakSync(breakId);
        //    BindingList<BookingDTO> bookingsDto = null;
        //    if (bookings != null)
        //    {
        //        bookingsDto = mapper.Map<List<BookingFromApi>, BindingList<BookingDTO>>(bookings);
        //    }

        //    return bookingsDto;

        //}
        private async void grdBookingsView_MasterRowGetChildListAsync(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            //async way of loading booking details
            GridView activeGridview = (GridView)sender;
            if (activeGridview.LevelName == "Breaks")
            {
                BindingList<BookingDTO> childList = new BindingList<BookingDTO>();
                e.ChildList = childList;
                int breakId = Convert.ToInt32(activeGridview.GetRowCellValue(e.RowHandle, "BreakId"));

                new System.Threading.Thread(async () => {
                    BindingList<BookingDTO> bookingList =  await GetBookingListASync(breakId);
                    // Add the loaded data to the childList list 
                    grdPlanning.BeginInvoke(new MethodInvoker(() => {
                        // This code is executed on the UI thread
                        Console.WriteLine(bookingList.Count);
                        foreach (BookingDTO booking in bookingList)
                            childList.Add(booking);
                        //childList = tempList;

                    }));
                }).Start();

                GridView breaksView = sender as GridView;
                int dayRowHandle = ((GridView)breaksView.ParentView).FocusedRowHandle;
                await ReloadBreak(breakId, dayRowHandle);


            }
        }
        private async Task<BindingList<BookingDTO>> GetBookingListASync(int breakId)
        {

            List<BookingFromApi> bookings = await apiCalls.GetBookingsForBreak(breakId);
            BindingList<BookingDTO> bookingsDto = null;
            if (bookings != null)
            {
                bookingsDto = mapper.Map<List<BookingFromApi>, BindingList<BookingDTO>>(bookings);
            }

            return bookingsDto;

        }
        private void grdBookingsView_MasterRowEmpty(object sender, MasterRowEmptyEventArgs e)
        {
            e.IsEmpty = false;
        }
        private void BreaksView_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }
        private void BreaksView_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            if (e.RelationIndex==0)
            {
                e.RelationName = "Bookings";
            }
        }
        private void BreaksView_RowClick(object sender, RowClickEventArgs e)
        {
            Console.WriteLine($"BreaksView_RowClick {e.RowHandle}");
            GridView detail = ((GridView)sender).GetDetailView(e.RowHandle, 0) as GridView;
        }
        private async void cmbOrders_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit l = (LookUpEdit)sender;

            order = await apiCalls.GetOrder(Convert.ToInt32(cmbOrders.EditValue));
            txtOrderProduct.Text = order.ProductName;
            txtOrderDescription.Text = order.Description;
            txtDemo.Text = order.DemographicName;
            txtOrderDescription.Text = order.Description;
            txtCurrency.Text = order.ISOCurrencyCode;

            txtOrderPeriod.Text = $"{order.StartDate.ToString("dd-MM-yy")} ==> {order.EndDate.ToString("dd-MM-yy")}";
            int orderId = Convert.ToInt32(l.EditValue);
            FillSpotGroups(orderId, order.OrderSpotGroups);
            cmbSpotGroup.ItemIndex = 0;
            FillOrderChannels(orderId, order.OrderChannels);
            //cmbChannels.ItemIndex = 0;
            int channelID = firstChannel;
            await LoadGrids(channelID);

        }
    }

}
