﻿using AMS_WinFormsClient.Helpers;
using AMS_WinFormsClient.Models;
using AMS_WinFormsClient.Properties;
using AMS_WinFormsClient.Services;
using AutoMapper;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMS_WinFormsClient
{
    public partial class frm_Test : Form
    {
        public bool AllowMultiInstance { get { return false; } }
        private IMapper mapper = null;
        private List<FeedBack> info = new List<FeedBack>();
        private ApiCalls apiCalls = new ApiCalls();

        public frm_Test()
        {
            InitializeComponent();
        }

        private async void frm_Test_Shown(object sender, EventArgs e)
        {
            spinEdit1.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            InitOrderBookingsGrid();
            InitMapper();
            cmbOrders.Properties.BeginUpdate();

            cmbOrders.Properties.ValueMember = "OrderId";
            cmbOrders.Properties.DisplayMember = "Description";

            cmbOrders.Properties.DataSource = await apiCalls.GetOrdersForCombo();
            cmbOrders.Properties.ForceInitialize();
            cmbOrders.Properties.PopulateColumns();
            cmbOrders.Properties.Columns["OrderId"].Visible = true;
            cmbOrders.Properties.DropDownRows = 24;
            cmbOrders.Properties.EndUpdate();
            cmbOrders.EditValue = 5;

            cmbChannels.Properties.BeginUpdate();

            cmbChannels.Properties.ValueMember = "ChannelId";
            cmbChannels.Properties.DisplayMember = "ChannelName";

            cmbChannels.Properties.DataSource = await apiCalls.GetChannelsForCombo();
            cmbChannels.Properties.ForceInitialize();
            cmbChannels.Properties.PopulateColumns();
            cmbChannels.Properties.Columns["ChannelId"].Visible = true;

            cmbChannels.Properties.DropDownRows = 24;
            cmbChannels.Properties.EndUpdate();
            cmbChannels.EditValue = 1;
        }
        private void InitMapper()
        {
            MapperConfiguration mapperconfig = new MapperConfiguration(cfg =>
            {

                var orderBookingMap = cfg.CreateMap<OrderBookingFromApi, OrderBookingDTO>();
                orderBookingMap.ForMember(destination => destination.Duration, opts => opts.MapFrom(x => TimeSpan.FromMilliseconds(x.Duration)));
            });
            mapperconfig.AssertConfigurationIsValid();
            mapper = mapperconfig.CreateMapper();
        }

        private void InitOrderBookingsGrid()
        {
            DevExpress.XtraGrid.Views.Grid.GridView orderBookingsView = this.grdOrderBookingsView;
            orderBookingsView.Columns.Clear();
            GridColumn c;
            c = orderBookingsView.Columns.Add();
            c.Caption = "BookingId";
            c.FieldName = "BookingId";
            c.Name = "BookingId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "OrderId";
            c.FieldName = "OrderId";
            c.Name = "OrderId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "ChannelName";
            c.FieldName = "ChannelName";
            c.Name = "ChannelName";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "BreakId";
            c.FieldName = "BreakId";
            c.Name = "BreakId";
            c.Width = 75;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Date";
            c.FieldName = "BreakDateTime";
            c.Name = "BreakDate";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "dd/MM/yy";
            c.OptionsColumn.ReadOnly = true;

            c.Visible = true;
            c = orderBookingsView.Columns.Add();
            c.Caption = "Time";
            c.FieldName = "BreakDateTime";
            c.Name = "BreakTime";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            c.DisplayFormat.FormatString = "HH:mm";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Pos";
            c.FieldName = "PositioninBreak";
            c.Name = "PositioninBreak";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Pref";
            c.FieldName = "IsPreferredPosition";
            c.Name = "IsPreferredPosition";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "OrderSpotGroupId";
            c.FieldName = "OrderSpotGroupId";
            c.Name = "OrderSpotGroupId";
            c.Width = 50;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "SpotLength";
            c.FieldName = "Duration";
            c.Name = "SpotLength";
            c.Width = 100;
            c.AppearanceCell.ForeColor = Color.Blue;
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "BasePrice";
            c.FieldName = "BasePrice";
            c.Name = "BasePrice";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "StartPrice";
            c.FieldName = "StartPrice";
            c.Name = "StartPrice";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Gross";
            c.FieldName = "Gross";
            c.Name = "Gross";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Surcharge";
            c.FieldName = "Surcharge";
            c.Name = "Surcharge";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Discount";
            c.FieldName = "Discount";
            c.Name = "Discount";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            c = orderBookingsView.Columns.Add();
            c.Caption = "Net";
            c.FieldName = "Net";
            c.Name = "Net";
            c.Width = 150;
            c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            c.DisplayFormat.FormatString = "#,0.00";
            c.OptionsColumn.ReadOnly = true;
            c.Visible = true;

            orderBookingsView.OptionsView.ColumnAutoWidth = false;
            orderBookingsView.OptionsDetail.AllowExpandEmptyDetails = true;
            orderBookingsView.OptionsBehavior.Editable = false;
            orderBookingsView.OptionsView.ShowIndicator = true;
            orderBookingsView.OptionsView.ShowGroupPanel = true;
            orderBookingsView.OptionsFilter.AllowFilterEditor = true;
            orderBookingsView.OptionsCustomization.AllowFilter = true;
            orderBookingsView.RowStyle += orderBookingsView_RowStyle;
        }

        private void orderBookingsView_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            GridView orderBookingsView = (GridView)sender;

            Double startPrice = Convert.ToDouble(orderBookingsView.GetRowCellValue(e.RowHandle, "StartPrice"));
            if (startPrice < 225)
            {
                e.Appearance.BackColor = Color.DarkGreen;
                e.Appearance.ForeColor = Color.White;
            }


        }

        private async void LoadOrderBookingsGrid(int orderId)
        {

            grdOrderBookings.DataSource = null;

            BindingList<OrderBookingDTO> orderBookings = await GetBookingsForOrderNotify(orderId);
            

           // List<OrderBookingsDTO> orderBookingsDto = mapper.Map<List<OrderBookingsFromApi>, List<OrderBookingsDTO>>(orderBookings);


            grdOrderBookings.BeginUpdate();
            grdOrderBookings.DataSource = orderBookings;
            grdOrderBookings.EndUpdate();

        }

        private void cmdLoadOrderBookings_Click(object sender, EventArgs e)
        {
            int orderId = (int)this.cmbOrders.EditValue;
            LoadOrderBookingsGrid(orderId);

        }
        private async Task<BindingList<OrderBookingDTO>> GetBookingsForOrderNotify(int orderId)
        {
            Uri address = new Uri(Settings.Default["Api_BaseHttp"].ToString() + "Bookings");

            var queryParams = "OrderId=" + orderId.ToString();

            string fileJsonString = "";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BEARER", Program.AppStorage.Bearer);

                var response = await client.GetAsync(address.ToString() + "?" + queryParams);

                if (response.IsSuccessStatusCode)
                {
                    fileJsonString = await response.Content.ReadAsStringAsync();
                }
            }

            List<OrderBookingFromApi> returnList = JsonConvert.DeserializeObject<OrderBookingFromApi[]>(fileJsonString).ToList();
            List<OrderBookingDTO> orderBookingsDto = mapper.Map<List<OrderBookingFromApi>, List<OrderBookingDTO>>(returnList);

            return new BindingList<OrderBookingDTO>(orderBookingsDto);
        }
        private void spinEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            
            if (grdOrderBookingsView.FocusedRowHandle >= 0)
            {
                //OrderBooking orderBooking = (OrderBooking)grdOrderBookingsView.GetFocusedRow();
                BindingList<OrderBookingDTO> orderBookings = grdOrderBookingsView.DataSource as BindingList<OrderBookingDTO>;

                OrderBookingDTO orderBooking = orderBookings.Where(s => s.BookingId == 44).FirstOrDefault();
                if (orderBooking !=null)
                {
                    if (orderBooking.StartPrice != null)
                    {
                        orderBooking.StartPrice += Convert.ToDouble(e.NewValue);
                    }
                }
            }
        }
    }
}
